#!/usr/bin/perl
use strict;
use warnings;
use DBI;
use GD::Image;
use Readonly;
use FindBin;

Readonly my $STATIC_PATH  => "$FindBin::Bin/../public";
Readonly my $UPLOAD_PATH  => "/images/upload";

my $dbh = DBI->connect('DBI:mysql:database=min_pod', 'root', '');

my $images = $dbh->selectall_arrayref('select * from images', {Slice => {}});

IMAGES:
for my $image (@$images) {
    my $orig_path   = $STATIC_PATH . $image->{img_url};
    my ($orig_name) = $image->{img_url} =~ m/([^\/]+)$/xms;
    next IMAGES if (!-f $orig_path);
    
    my $orig_image  = GD::Image->new($orig_path) or die;
    my $orig_width  = $orig_image->width;
    my $orig_height = $orig_image->height;
    
    my ($new_width, $new_height) = calculate_size($orig_width, $orig_height, 800, 600);
    my $new_image = GD::Image->new($new_width, $new_height);
    $new_image->copyResized($orig_image, 0, 0, 0, 0, $new_width, $new_height, $orig_image->getBounds);    
    my $image_name = write_image($new_image);
    undef $new_image;
    
    my $thumb_height = 231 * $orig_height / $orig_width;
    my $thumb_image  = GD::Image->new(231, $thumb_height);
    $thumb_image->copyResized($orig_image, 0, 0, 0, 0, 231, $thumb_height, $orig_image->getBounds);
    my $thumb_name = write_image($thumb_image);
    undef $thumb_image;
    
    my $query = 'update images
                 set thumb_url     = ?,
                     img_url       = ?,
                     original_path = ?,
                     img_path      = ?,
                     thumb_path    = ?
                 where id = ?';
    my @bind  = ("$UPLOAD_PATH\/$thumb_name",
                 "$UPLOAD_PATH\/$image_name",
                 "$STATIC_PATH\/$UPLOAD_PATH\/$orig_name",
                 "$STATIC_PATH\/$UPLOAD_PATH\/$image_name",
                 "$STATIC_PATH\/$UPLOAD_PATH\/$thumb_name",
                 $image->{id});
    
    use Data::Dumper;
    
    warn Dumper($query);
    warn Dumper(@bind);
    $dbh->do($query, undef, @bind);
    
}

sub generate_filename {
    my $ext = shift;
    my $rand_num = int(rand 100000);
    
    my ($sec, $min, $hour, $mday, $month, $year) = localtime;
    $month = $month + 1;
    $year  = $year + 1900;

    my $name = sprintf(
        "image-%04s%02s%02s%02s%02s%02s-%05s.%s",
        $year, $month, $mday, $hour, $min, $sec, $rand_num, $ext
    );

    my $image_path = $STATIC_PATH . $UPLOAD_PATH . "/$name";
    
    return $name unless ( -f $image_path );    
    return generate_filename( $ext );    
}

sub write_image {
    my ($image) = @_;

    my $file_name = generate_filename('png');
    
    open my $out, '>', $STATIC_PATH . $UPLOAD_PATH . "/$file_name"
        or die "BLYA";
    print $out $image->png;
    close $out;

    return $file_name;
}


sub calculate_size {    
    my ($width, $height, $want_width, $want_height) = @_;
    my ($new_width, $new_height) = ($want_width, $want_height);
    my $RATIO = $want_width / $want_height;
    
    if ($width / $height < $RATIO) {
        $new_width  = 600 * $width / $height; 
    }
    else {
        $new_height = 800 * $height / $width;
    }

    return ($new_width, $new_height);
}
