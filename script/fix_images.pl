#!/usr/bin/perl

use GD;
use DBI;
use Data::Dumper;

my $dsn = 'dbi:mysql:minpod:localhost';
my $dbh = DBI->connect($dsn, 'root', '', { RaiseError => 1, AutoCommit => 1 });
$dbh->{mysql_enable_utf8} = 1;

$dbh->do("set names 'utf8';");

my $list = $dbh->selectall_arrayref('select id, img from products where img like "/img/%"', {Slice =>{}});

#print Dumper(@$list);


my $IMAGE_DIR = "/home/yakudza/devel/min_pod/public/img";
my $IMAGE_DIR2 = "/home/yakudza/devel/min_pod/public/images";

for my $prod (@$list)
{
    
    $prod->{img} =~ s/\/img\///;
    my $thumb_name = create_thumb($prod->{img}, $IMAGE_DIR, $IMAGE_DIR2 . '/product_border.png');    
    
    $dbh->do('UPDATE products SET img_small = ? where id = ? ', undef, '/img/thumb_' . $thumb_name, $prod->{id});
    
    print $thumb_name .  Dumper ($prod);
}





sub create_thumb
{
    my ($im_filename, $path, $wm_path) = @_;
    
    my $thumb_filename = $im_filename;
    $thumb_filename =~ s/\..*$/\.png/;
    my $outfile = $path . '/thumb_' . $thumb_filename;
    GD::Image->trueColor(1);

    
    my $im = GD::Image->new( $path . '/'. $im_filename) or 
        die(sprintf("Failed to open \"%s\"", $path . '/'. $im_filename));        
    my $wm = GD::Image->new( $wm_path ) or
        die(sprintf("Failed to open \"%s\"", $wm_path));;
    $wm->saveAlpha(1);
    my $im_h = int (218 * $im->height / $im->width);
    my $wm_h = int (231 * $im->height / $im->width);
    
    my $image = GD::Image->new(231, $wm_h);

    my $clear = $image->colorAllocateAlpha(255, 255, 255, 127);
    $image->transparent($clear);
    

    $image->alphaBlending(0);
    $image->saveAlpha(1);
    $image->fill(50,50,$clear);
    $image->alphaBlending(1);
    $image->saveAlpha(1);
    

    $image->copyResized($im, 5, 1, 0, 0, 220, $im_h, $im->width, $im->height);
    $image->copyResized($wm, 0, 0, 0, 0, 231, $wm_h, $wm->width, $wm->height);

    open my $out, ">$outfile";
    print $out $image->png;
    close $out;
    
    return $thumb_filename;
}

sub create_filename
{
    my ($sec, $min, $hour, $mday, $month, $year) = localtime;
    
    $month = $month + 1;
    $year = $year + 1900;
    
    my $rand_num = int(rand 100000);
    
    my $name = sprintf("image-%04s%02s%02s%02s%02s%02s-%05s",
                       $year, $month, $mday, $hour, $min, $sec, $rand_num);
    
    return $name;    
}
