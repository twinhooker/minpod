#!/usr/bin/perl
use common::sense;
use File::Find;
use DBI;
use Path::Tiny;
use Getopt::Long;
use Imager;
use Try::Tiny;

GetOptions(
    "dsn=s"      => \my $dsn,
    "username=s" => \my $username,
    "password=s" => \my $password,
) or die "shit";

my $dbh = DBI->connect( $dsn, $username, $password || '', { RaiseError => 1, AutoCommit => 1 } );
my $image_list = $dbh->selectall_arrayref('select id, img_url, thumb_url, img_path, thumb_path from images order by id desc', { Slice => {} } );
my $public_dir = Path::Tiny->cwd->child( 'public' );

IMAGES:
for my $image ( @$image_list ) {
    my $rel_path = path $image->{img_url};
    my $filename = $rel_path->basename;
    my ( $original_name_pattern ) = $filename =~ m/( image-\d+ )/msx;
    my $original_name;
    find sub {
        $original_name = $File::Find::name if (  m/( original_$original_name_pattern ) /msx );
    }, $public_dir->child('images/upload');

    next IMAGES unless $original_name;

    try {
        say "Converting $original_name -> $image->{img_path} => 500x500";
        scale_image( from => $original_name, to => $image->{img_path}, x => 500, y => 500 );
        say "Converting $original_name -> $image->{thumb_path} => 355x355";
        scale_image( from => $original_name, to => $image->{thumb_path}, x => 355, y => 355 );
    } catch {
        say "Got Error:" . $_;
    };
}

sub scale_image {
    my %args = @_;
    my $original_path  = $args{from};

    try {
        my $img = Imager->new( file => $args{from} ) or die Imager->errstr;
        my $scaled = $img->scale(
            ( xpixels => $args{x} ) x!! $args{x},
            ( ypixels => $args{y} ) x!! $args{y},
            type => 'min'
        );
        $scaled->write( file => $args{to} ) or die $scaled->errstr;
    }
    catch {
        say sprintf( "Error: %s.", $_ );
    };

    return;
}


