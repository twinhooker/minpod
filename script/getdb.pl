#!/usr/bin/perl
use utf8;
use strict;
use Mojo::UserAgent;
use Data::Dumper;
use Encode qw/encode decode/;
use Cwd;
use File::Basename;
use URI;

use DBI;


my $ua = Mojo::UserAgent->new;

my $url_main = 'http://министерство-подарков.рф/';

my $res = $ua->get($url_main . '?1')->res;


my $cats;

my $dsn = 'dbi:mysql:minpod:localhost';
my $dbh = DBI->connect($dsn, 'root', '', { RaiseError => 1, AutoCommit => 1 });
$dbh->{mysql_enable_utf8} = 1;

$dbh->do("set names 'utf8';");

$res->dom
    ->find('#heihNeed table a')
    ->each(sub {
        my $a = shift;
        my $cat_name;
        if ($a->text eq '')
        {
            $cat_name = encdec($a->at('nobr')->text);

            $cats->{$cat_name} = $a->attrs('href');# . "\n";#    print encode ();
        }
        else
        {
            $cat_name = encdec($a->text);
            $cats->{$cat_name} = $a->attrs('href');
        }
    });
    


for my $name (keys %{$cats})
{
    my $cat = $ua->get('http://министерство-подарков.рф/' .  $cats->{$name})->res->dom;
    
    $dbh->do('insert into categories (name) values (?)', undef, $name);
    
    my $cat_id = $dbh->last_insert_id(undef, undef, 'categories', 'id');
    
    $cat->find('td.tabContent')->each(sub {
        my $tab = shift;
        
        my $products;
        
        my $desc = get_child_desc ($tab);
        
        my ($name) = ($desc =~ m/Название:\s([^\<]*)/);
        
        my $img = (defined $tab->at('a')) ? $tab->at('a')->attrs('href') : '';
        my $img_thumb = $tab->at('img')->attrs('src');
        
        download_img($img);
        download_img($img_thumb);
        
        
        push @$products, encode('utf8',$name) || encdec($tab->at('strong')->text); #name
        push @$products, $tab->at('span.price')->text;   # price 
        
        push @$products, '/'. $img;                  # img
        push @$products, '/' . $img_thumb;            #img_thumb

        $desc =~ m/Код:\s(\d+)/;            # code
        push @$products, $1;
        
        my ($rd) = ($desc =~ m/Описание:\s([^\<]*)/);   # desc
        push @$products, encode('utf8',$rd);
        
        my ($pack) = ($desc =~ m/Упаковка:\s([^\<]*)/);         #packing
        push @$products, encode('utf8', $pack);
        
        my ($height) = ($desc =~ m/Высота:\s([^\<]*)/);         # height
        push @$products, encode('utf8', $height);
        
        my ($material) = ($desc =~ m/Материал:\s([^\<]*)/);     # material
        push @$products, encode('utf8', $material);
        
        push @$products, $cat_id;
        
        # insert to db
        
        $dbh->do('insert into products (name, price , img, img_small, code, description, packing, height, material, cat_id)
                 values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', undef, @$products);
        
        
    });
}

sub download_img
{
    my $path = shift;
    
    my $home = getcwd;
    my $img_dir = $home . '/../public/img/';
    
    my $url = URI->new($url_main. $path);
    
    my $res = $ua->get($url->as_string, { Accept => '*/*' })->res;
    print "bla bla \n";
    if ($res->code == 200)
    {
        my $filename = basename ($url->path);
        open my $fh , '>' . $img_dir . $filename;
        print $fh $res->body;
        close $fh;
        print "$filename done\n";
    }
}


sub get_child_desc
{
    my $tab = shift;
    
    my $shi = (defined $tab->children('div')->[1]) ? $tab->children('div')->[1]->to_xml : $tab->children('div')->[0]->to_xml;
    
    decode('cp1251',$shi);
}



sub encdec {
    encode('utf8',decode('cp1251', shift));
}