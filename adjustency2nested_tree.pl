#!/usr/bin/perl
use strict;
use warnings;

use DBI;
use Data::Dumper;

my $dbh = DBI->connect("dbi:mysql:database=minpod", 'minpod', 'inflectmix');


my $cats = $dbh->selectall_arrayref("select * from categories order by id", { Slice => {}});
#warn Dumper( $cats );

#$dbh->do("update categories set left_key = 1, right_key = ? where id = 1", undef, scalar(@$cats) + 2);
my $i = 1;
for my $cat (@$cats) {
    $dbh->do(
        "update categories set left_key = ?, right_key = ? where id = ?",
        undef, $i++, $i++, $cat->{id}
    );
}
