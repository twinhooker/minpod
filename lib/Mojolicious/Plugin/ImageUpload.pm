package Mojolicious::Plugin::ImageUpload;
use Try::Tiny;
use Path::Tiny;
use Imager;
use Readonly;

use Mojo::Base 'Mojolicious::Plugin';
use Exception::Class (
    'ImageType',
);

Readonly my $UPLOAD_PATH  => "/images/upload";

has 'image_path';

sub register {
    my ( $self, $app, $conf ) = @_;


    my $static_dir = path ( $app->home, 'public' );
    my $image_dir  = path ( $static_dir, $UPLOAD_PATH );
    $self->image_path( $image_dir );

    $app->helper( save_image       => sub { shift; $self->save_image( @_ ) } );
    $app->helper( create_thumbnail => sub { shift; $self->create_thumbnail( @_ ) } );

    $app->helper( image_static_path => sub { $static_dir } );
    $app->helper( image_upload_path => sub { $UPLOAD_PATH } );
}

sub save_image {
    my ( $self, $upload, $opts ) = @_;

    my $ext          = $self->get_extension( $upload );
    my $original_path = $self->generate_filepath( $ext, 'original_' );
    $upload->move_to( $original_path->stringify );

    my $path = $original_path;
    if ( $opts && !$opts->{skip_scale} ) {
        my $main_img_path = $self->generate_filepath( $ext );

        $path = $self->scale_image(
            from => $original_path,
            to   => $main_img_path,
            x    => 500,
            y    => 500,
        );
    }

    return $path;
}

sub create_thumbnail {
    my ( $self, $image_path ) = @_;

    my $thumbnail_path = path( $image_path->dirname, 'thumb_' . $image_path->basename );
    my $path = $self->scale_image(
        from => $image_path,
        to   => $thumbnail_path,
        x    => 355,
        y    => 355,
    );

    return $path;
}

sub scale_image {
    my ( $self, %args ) = @_;
    my $original_path  = $args{from};
    use Data::Dumper;
    warn Dumper( \%args );
    my $img = Imager->new( file => $args{from} ) or die Imager->errstr;
    my $scaled = $img->scale(
        ( xpixels => $args{x} ) x!! $args{x},
        ( ypixels => $args{y} ) x!! $args{y},
        type => 'min'
    );
    $scaled->write( file => $args{to} ) or die $scaled->errstr;

    return $args{to};
}

sub generate_filepath {
    my ( $self, $extension, $prefix ) = @_;
    my ( $sec, $min, $hour, $mday, $month, $year ) = localtime;

    $month    = $month + 1;
    $year     = $year + 1900;
    $prefix ||= '';
    my $rand_num = int( rand 100000 );

    my $name = sprintf( "%simage-%04s%02s%02s%02s%02s%02s-%05s",$prefix, $year, $month, $mday, $hour, $min, $sec, $rand_num );
    my $path = path( $self->image_path . "/$name\." . $extension );

    return ( $path->exists ) ? $self->generate_filepath( $extension ) : $path;
}

sub get_extension {
    my ( $self, $upload ) = @_;

    my %extensions = (
        'image/gif'  => 'gif',
        'image/jpeg' => 'jpg',
        'image/png'  => 'png'
    );
    my %valid_types
        = map {$_ => 1} qw(image/gif image/jpeg image/png);

    my $upload_type = $upload->headers->content_type;
    ImageType->throw()
        unless ( $valid_types{$upload_type} );

    return $extensions{$upload_type};
}



1;
