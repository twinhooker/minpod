package MinPod;

use utf8;
use common::sense;
use Mojo::Base 'Mojolicious';
use Mojolicious::Plugin::Authentication;
use Mojolicious::Plugin::Validator;
use DBIx::Custom;
use DBIx::Connector;
use Email::Valid;

has conn => sub {
    my $self = shift;

    my $data_source = "dbi:mysql:database=minpod";
    my $user        = 'minpod';
    my $password    = 'inflectmix';

    my $connector = DBIx::Connector->new(
        $data_source, $user, $password,
        {
            RaiseError        => 1,
            AutoCommit        => 1,
            mysql_enable_utf8 => 1,
            %{ DBIx::Custom->new->default_option }
        },
    );
    my $conn = DBIx::Custom->new(connector => $connector);
    $conn->include_model('Model' => [
        'products',
        'categories',
        'articles',
        'users',
        'orders',
        'orders_products',
        'mmail',
        'settings',
        'mmail_file',
        'tags',
        'images',
    ]);

    return $conn;
};

# This method will run once at server start
sub startup {
    my $self = shift;

    $self->helper(dbi => sub { $self->app->conn });
    $self->app->config(
        hypnotoad => {
            listen => [
                'http://127.0.0.1:8080',
                # 'https://*:443?cert=./crt/ministerstvo-podarkov.com.crt&key=./crt/ministerstvo-podarkov.com.key',
                # 'http://*:80',
            ]
        }
    );

    $self->plugin('validator');
    $self->plugin('ImageUpload');

    $self->helper(v_err => sub {
        my ($self, $pname, $class) = @_;

        if (my $errs = $self->stash('errmsg'))
        {
            if ($errs->{$pname})
            {
                return 'error' if ($class); 
                return '<span class="error_msg red">'. $errs->{$pname} .'</span>';
            }
        }
    });

    # Validator
    $self->validator->add_check(
        email => sub {
            my ($validation, $name, $value) = @_;
            my $address = Email::Valid->address($value);
            return $address ? 0 : 1;
        }
    );
    $self->validator->add_check(
        phone => sub {
            my ($validation, $name, $value) = @_;
            $value =~ s/[^0-9]//gmsx;
            return ( $value =~ m/\d{6,12}/msx ) ? 0 : 1;
        }
    );
    $self->helper(
        menu_active => sub {
            my $self = shift;
            my $pth = \@_;

            my $path = $self->req->url->path;

            my @split_path = split(/\//, $path);

            shift @split_path for 1..2;

            foreach my $part (@$pth) {
                my $path_part = shift(@split_path);
                return 0 unless ($path_part && $part eq $path_part);
            }
            return 1;
        }
    );

    $self->plugin('authentication' => {
        session_key => 'cust',

        load_user => sub {
            my ($self, $id) = @_;
            $self->dbi->model('users')->get_user($id);
        },
        validate_user => sub {
            my ($self, $username, $password) = @_;
            $self->dbi->model('users')->auth($username, $password);
        }
    });

    # laod routes 
    $self->_load_routes;
}

sub _load_routes {
    my $self = shift;

    # Routes
    my $r = $self->routes;
    $r->any('/')->to('min_pod#main');
    $r->any('/75r90ro6vbmi68mbois0lpxe1urqux.html')->to( cb => sub { shift->reply->static( '75r90ro6vbmi68mbois0lpxe1urqux.html' ) } );
    $r->any('/4am3647e2jlrq8l8uaawfzn7mwjc0w.html')->to( cb => sub { shift->reply->static( '4am3647e2jlrq8l8uaawfzn7mwjc0w.html' ) } );
    $r->any('/categories/')->to('categories#list')->name('category_list');
    $r->any('/categories/:id', [ id => qr/\d+/ ] )->to('categories#show')->name('show_category');
    $r->any('/products/:id',   [ id => qr/\d+/ ] )->to('products#show')->name('show_product');
    $r->any('/tags/:id',       [ id => qr/\d+/ ] )->to('min_pod#show_tag')->name('show_tag');

    $r->any('/news')->to('min_pod#news')->name('news');
    $r->any('/contacts')->to('min_pod#contacts')->name('contacts');
    $r->any('/useful')->to('min_pod#useful')->name('useful');
    $r->any('/conditions')->to('min_pod#conditions')->name('conditions');
    $r->any('/delivery')->to('min_pod#delivery')->name('delivery');
    $r->any('/article/:id', id => qr/\d+/)->to('min_pod#article')->name('article');
    $r->any('/about')->to('min_pod#about')->name('about');
    $r->any('/search')->to('min_pod#search')->name('search');
    $r->any('/suggest')->to('min_pod#suggest')->name('suggest');
    $r->any('/weather')->to('min_pod#weather_proxy')->name('weather');

    #
    # cart routes
    #
    $r->any('/show_cart')->to('cart#show_cart')->name('cart');
    $r->any('/clear_cart')->to('cart#clear')->name('cart_clear');

    my $cart = $r->under('/cart')->to('cart#cart');
    $cart->post('/add')->to('cart#add')->name('cart_add');
    $cart->post('/modify')->to('cart#modify')->name('cart_modify');
    $cart->get('/delete')->to('cart#delete')->name('cart_delete');
    $r->any('/cart_submit')->to('cart#submit')->name('cart_submit');
    $r->any('/order_cart')->to('cart#order')->name('cart_order');
    
    # auth routes
    $r->any('/reg')->to('users#reg')->name('reg');
    $r->any('/unscribe/:user_hash')->to('users#unscribe');
    
    my $mda = $r->under('/managment')->to('mda#auth');
    $r->any('/login')->to('mda#login')->name('mda_login');
    $r->any('/logout')->to('mda#logout')->name('mda_logout');

    # products categories
    $mda->any('/')->to('mda#main');
    $mda->any('/save_settings')->to('mda#save_settings')->name('mda_save_settings');

    $mda->any('/move_cat')->to('mda-cats#move_cat')->name('mda_move_cat');
    
    $mda->any('/cats/list')->to('mda-cats#mda_list')->name('cats_mda_list');
    $mda->any(['GET']    => '/cats/')->to('mda-cats#mda_edit')->name('cats_mda_new');
    $mda->any(['POST']   => '/cats/')->to('mda-cats#add')->name('mda_cats_add');
    $mda->any(['POST']   => '/cats/:id', [ id => qr/\d+/ ])->to('mda-cats#save')->name('mda_cats_save');
    $mda->any(['DELETE'] => '/cats/:id', [ id => qr/\d+/ ])->to('mda-cats#remove')->name('mda_cats_remove');

    $mda->any('/cats/:id/delete', [ id => qr/\d+/ ])->to('mda-cats#delete')->name('mda_cats_delete');
    $mda->any('/cats/:cat_id/products/', [ cat_id => qr/\d+/ ])->to('mda-products#mda_edit')->name('prods_mda_cnew');
    $mda->any('/cats/:cat_id/set_img/:p_id', [ cat_id => qr/\d+/, p_id => qr/\d+/ ])->to('mda-cats#set_image')->name('mda_cats_setimage');
    $mda->any('/cats/:id/up', [ id => qr/\d+/ ])->to('mda-cats#pos_up')->name('mda_cats_posup');
    $mda->any('/cats/:id/down', [ id => qr/\d+/ ])->to('mda-cats#pos_down')->name('mda_cats_posdown');

    $mda->any('/cats/:cat_id/products/list')->to('mda-products#mda_list')->name('prods_mda_list');

    $mda->any(['POST'] => '/products/')->to('mda-products#add')->name('mda_products_save');
    $mda->any(['POST'] => '/products/:id')->to('mda-products#save');
    $mda->any('/products/:id/delete', [ id => qr/\d+/ ])->to('mda-products#mda_delete')->name('prods_mda_delete');
    $mda->any('/products/:id/up', [ id => qr/\d+/ ])->to('mda-products#pos_up')->name('prods_mda_posup');
    $mda->any('/products/:id/down', [ id => qr/\d+/ ])->to('mda-products#pos_down')->name('prods_mda_posdown');
    $mda->any('/products/move')->to('mda-products#move')->name('mda_products_move');

    $mda->any('/products/add_img')->to('mda-products#mda_add_img')->name('prods_mda_addimg');
    $mda->any('/products/del_img')->to('mda-products#mda_del_img')->name('prods_mda_delimg');
    $mda->any('/products/:id/toggle', [ id => qr/\d+/ ])->to('mda-products#toggle')->name('prods_mda_toggle');
    $mda->any('/products/:id/toggle_new', [ id => qr/\d+/])->to('mda-products#toggle_new')->name('prods_mda_toggle_new');
    $mda->any('/products/actions')->to('mda-products#actions')->name('mda_products_actions');
    
    
    $mda->any('/new/')->to('mda-cats#list_new')->name('list_new');
    
    $mda->any('/new/:id/up', [ id => qr/\d+/ ])->to('mda-products#new_pos_up')->name('newprods_mda_posup');
    $mda->any('/new/:id/down', [ id => qr/\d+/ ])->to('mda-products#new_pos_down')->name('newprods_mda_posdown');
    $mda->any('/new/:id/set_image', [ id => qr/\d+/ ])->to('mda-products#new_set_image')->name('mda_new_setimage');
    $mda->any('/new/:id/remove', [ id => qr/\d+/ ])->to('mda-products#new_remove')->name('mda_new_remove');
    
    # $mda->any('/mmail/send/:tid', [ tid => qw/\d+/ ])->to('mda-mmail#send_template')->name('mda_mmail_send_template');
    # $mda->any('/mmail/save_settings')->to('mda-mmail#save_settings')->name('mda_mmail_save_settings');
    # $mda->any('/mmail/tmpl/')->to('mda-mmail#list_template')->name('mda_mmail_list_template');
    # $mda->any('/mmail/tmpl/addfile/')->to('mda-mmail#upload_file')->name('mmail_attach_file');
    # $mda->any(['GET'] => '/mmail/tmpl/:id')->to('mda-mmail#show_template', id => qw/\d+/)->name('mda_mmail_show_template');
    # $mda->any(['POST'] => '/mmail/tmpl/:id')->to('mda-mmail#write_template', id => qw/\d+/)->name('mda_mmail_write_template');

    # $mda->any('/mmail/remove/:id')->to('mda-mmail#remove_template')->name('mda_mmail_remove_template');
        
    # $mda->any('/mmail/email/')->to('mda-mmail-email#list')->name('mda_mmail_email_list');
    # $mda->any('/mmail/email/:gid', gid => qw/\d+/)->to('mda-mmail-email#list')->name('mda_mmail_email_grp_list');
    # $mda->any('/mmail/email/switch/:id', id => qw/\d+/)->to('mda-mmail-email#switch')->name('mda_mmail_email_switch');
    # $mda->any('/mmail/email/remove/:id', id => qw/\d+/)->to('mda-mmail-email#remove_email')->name('mda_mmail_remove_email');
    # $mda->any('/mmail/email/add_group')->to('mda-mmail-email#add_group')->name('mda_mmail_email_addgrp');
    # $mda->any('/mmail/email/remove_group/:id', id => qw/\d+/)->to('mda-mmail-email#remove_group')->name('mda_mmail_remove_group');
    # $mda->any('/mmail/email/import')->to('mda-mmail-email#import')->name('mda_mmail_email_import');


    $mda->any('/articles/list')->to('mda-articles#list')->name('mda_articles_l');
    $mda->any('/articles/list/:id', [ id => qr/\d+/ ])->to('mda-articles#list')->name('mda_articles_list');
    $mda->any('/articles/')->to('mda-articles#edit')->name('mda_articles_new');
    $mda->any('/articles/:id', [ id => qr/\w+/ ])->to('mda-articles#edit')->name('mda_articles_edit');
    $mda->any('/articles/:id/delete', [ id => qr/\d+/ ])->to('mda-articles#delete')->name('mda_articles_delete');
    $mda->any('/articles/addimg')->to('mda-articles#addimg')->name('mda_articles_addimg');

    $mda->any('/orders')->to('mda-orders#list')->name('mda_orders_list');
    $mda->any('/orders/show/:id', [ id => qr/\d+/ ])->to('mda-orders#show')->name('mda_orders_show');
    $mda->any('/orders/delete/:id', [ id => qr/\d+/ ])->to('mda-orders#delete')->name('mda_orders_delete');
    $mda->any('/orders/complete/:id', [ id => qr/\d+/ ])->to('mda-orders#complete')->name('mda_orders_complete');

    $mda->any('/users')->to('mda-users#list')->name('mda_users_list');
    $mda->any('/users/:id/delete', [ id => qr/\d+/ ])->to('mda-users#delete')->name('mda_users_delete');  

    $mda->get('/tags')->to('mda-tags#list')->name('mda_tags_list');
    $mda->get('/tags/create')->to('mda-tags#edit')->name('mda_tags_create');
    $mda->get('/tags/:id')->to( 'mda-tags#edit')->name('mda_tags_edit');
    $mda->post('/tags')->to('mda-tags#add')->name('mda_tags_add');

    $mda->post('/tags/:id')->to('mda-tags#update')->name('mda_tags_update');

    $mda->any('/tags/:id/delete', [ id => qr/\d+/ ])->to('mda-tags#remove')->name('mda_tags_remove');
    $mda->any('/tagged_product/:tid/:pid', [ pid => qr/\d+/, tid => qr/\d+/])->to('mda-tags#remove_tagged')->name('mda_tagged_product_remove');
    $mda->any('/tags/:id/up', [ id => qr/\d+/ ])->to('mda-tags#pos_up')->name('mda_tags_posup');
    $mda->any('/tags/:id/down', [ id => qr/\d+/ ])->to('mda-tags#pos_down')->name('mda_tags_posdown');
    $mda->any('/tags/:id/tagged_down', [ id => qr/\d+/ ])->to('mda-tags#tagged_down')->name('mda_tagged_product_posdown');
    $mda->any('/tags/:id/tagged_up', [ id => qr/\d+/ ])->to('mda-tags#tagged_up')->name('mda_tagged_product_posup');

    $mda->any('/gallery')->to('mda-gallery#main')->name('mda_gallery');
    $mda->any('/gallery_tree/')->to('mda-gallery#nested_jstree')->name('mda_gallery_tree');
    $mda->any('/gallery_tree_as_list')->to('mda-gallery#tree_as_list')->name('mda_gallery_tree_as_list');


    $mda->any('/gallery/:cat_id', [ cat_id => qr/\d+/ ])->to('mda-gallery#product_list')->name('mda_gallery_product_list');
    $mda->any(['GET'] => '/products/:id', [ id => qr/\d+/ ])->to('mda-gallery#show_product')->name('mda_gallery_show_product');

    $mda->post('/images')->to('mda-images#upload')->name('mda_images_add');

    $mda->get('/export')->to('mda-export#form')->name('mda_export_form');
    $mda->post('/export')->to('mda-export#do')->name('mda_export_do');
}

1;
