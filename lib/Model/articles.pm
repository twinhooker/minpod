package Model::articles;

use DBIx::Custom::Model -base;

has primary_key => sub { [ 'id' ] };

sub list
{
    my ($self, $filter) = @_;

    my $res = $self->select(where => $filter, append => 'order by date_created desc')->fetch_hash_all;
}

sub get_article
{
    my ($self, $id) = @_;
    
    my $res = $self->select(where => { id => $id })->one;
    
    scalar keys %{$res} ? $res : undef;
}

sub get_about
{
    my $self = shift;
    
    my $res = $self->select(where => {cat_id => 5} )->one;
    
    scalar keys %{$res} ? $res : undef;
}

sub get_contacts
{
    my $self = shift;
    
    my $res = $self->select(where => {cat_id => 3} )->one;
    
    scalar keys %{$res} ? $res : undef;
}

sub get_conditions
{
    my $self = shift;
    
    my $res = $self->select(where => {cat_id => 4} )->one;
    
    scalar keys %{$res} ? $res : undef;
}

sub up
{
    my ($self, $params, $id) = @_;
    
    my $cols = ['title', 'cat_id', 'body', 'meta_tags', 'preview'];
    
    my $v_params;
    for my $key (@$cols)
    {
        $v_params->{$key} = $params->{$key} if (exists($params->{$key}));
    }
    
    my $res;
    if (defined $id)
    {
        $res = $self->update($v_params, where => { id => $id});
    }
    else
    {
        $res = $self->insert($v_params);
    }
    
    return undef unless ($res == 1);
    
    $self->connector->dbh->last_insert_id(undef, undef, 'articles', 'id');
}

sub get
{
    my ($self, $id) = @_;
    
    my $res = $self->select(where => { id => $id })->one;
    
    scalar keys %{$res} ? $res : undef;
}

sub rem
{
    my ($self, $id) = @_;
    
    my $res = $self->delete(where => { id => $id });
    
    $res == 1 ? $res : undef;
}

1;
