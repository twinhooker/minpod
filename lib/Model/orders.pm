package Model::orders;
use POSIX qw (strftime);
use DBIx::Custom::Model -base;

has primary_key => sub { [ 'id' ] };

sub add {
    my ( $self, $user_id, $cart ) = @_;

    my $time   = strftime "%Y-%m-%d %H:%M:%S", localtime;
    my $order  = { user_id => $user_id, progress => 0, created => $time };
    my $result = $self->insert( $order );

    return undef unless ( $result == 1 );

    my $order_id = $self->dbi->dbh->last_insert_id( undef, undef, 'orders', 'id' );

    return $order_id;
}

sub list {
    my ( $self ) = @_;

    my $res = $self->dbi->dbh->selectall_arrayref( 'select * from orders order by created desc', { Slice => {} } );
}

sub get_order {
    my ( $self, $order_id ) = @_;

    my $res = $self->dbi->dbh->selectall_arrayref(
        "SELECT p.name, p.code, op.quantity, coalesce( nullif( p.new_price, 0 ), p.price ) as price
         FROM products as p
         JOIN orders_products as op ON op.product_id = p.id
         WHERE op.order_id = ?
         ORDER BY p.cat_id",
        { Slice => {} },
        $order_id
    );
}

sub remove {
    my ( $self, $id ) = @_;

    my $result = $self->delete( where => { id => $id } );

    return ( $result == 1 ) ? 1 : undef;
}

sub complete {
    my ( $self, $id ) = @_;

    my $result = $self->dbi->dbh->do(
        "update orders set progress = 1 where id = ? ",
        undef, $id
    );

    return ( $result == 1 ) ? 1 : undef;
}

1;
