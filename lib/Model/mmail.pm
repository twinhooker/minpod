package Model::mmail;
use common::sense;
use DBIx::Custom::Model -base;

has primary_key => sub { [ 'id' ] };

sub list_templates
{
    my $self = shift;
    my $res  = $self->connector->dbh->selectall_arrayref('select * from mmail_templates', {Slice => {}});
}

sub get_template
{
    my ($self, $id) = @_;
    
    return undef unless($id);
    my $res = $self->connector->dbh->selectall_arrayref('select * from mmail_templates where id = ?',
                                                        {Slice => {}},
                                                        $id);
    
    my $result = scalar @$res ? $res->[0] : undef;
    
    if ($result)
    {
        $result->{attach} = [];
        $result->{attach} = $self->connector->dbh->selectall_arrayref('select id, name, path
                                                                       from mmail_file
                                                                       where tid = ?',
                                                                       {Slice => {}}, $result->{id});
    }

    return $result;
}

sub write_template
{
    my ($self, $id, $params) = @_;
    
    my $res;
    if ($id)
    {
        $res = $self->connector->dbh->do('update mmail_templates set title = ?, body = ? where id = ?',
                                         undef,
                                         $params->{title},
                                         $params->{body},
                                         $id);
    }
    else
    {
        $res = $self->connector->dbh->do('insert into mmail_templates (title, body) values (?,?)',
                                         undef,
                                         $params->{title},
                                         $params->{body});
    }
    
    return 0 unless (defined $res && $res ne '0E0');
    my $new_id = ($id != 0) ? $id : $self->connector->dbh->last_insert_id(undef, undef, 'mmail_templates', 'id');
    
    my @attach;
    
    for my $param (keys %$params)
    {
        if ($param =~m/attach(\d)/)
        {
            push @attach, $params->{$param};
        }
    }
    if (scalar @attach)
    {
        my $ph = join ( ',', ('?') x scalar (@attach));
        $self->connector->dbh->do("update mmail_file set tid = ? where id in ($ph)", undef, $new_id, @attach);
    }
    
    return $new_id;
}

sub remove_template
{
    my ($self, $id) = @_;
    
    my $res = $self->connector->dbh->do('delete from mmail_templates where id = ?', undef, $id);
    return 0 unless (defined $res && $res ne '0E0');
    
    $res;
}

sub remove_email
{
    my ($self, $id) = @_;
    
    my $res = $self->connector->dbh->do('delete from mmail_emails where id = ?', undef, $id);
    return 0 unless (defined $res && $res ne '0E0');
    
    $res;
}

sub list_emails
{
    my ($self, $gid, $offset) = @_;
    
    my $query = 'select * from mmail_emails where gid = ?';
    
    my @bind;
    push @bind, $gid;
    
    #if (defined $q && $q ne '')
    #{
    #    $query .= ' where email = ?';
    #    push @bind, '%' . $q . '%';
    #}
    
    $offset  = 0 unless defined $offset;
    $offset *= 50;
    
    $query .= ' limit ?, ?';
    push @bind, $offset, 50;
    
    my $result = $self->connector->dbh->selectall_arrayref($query, {Slice => {}}, @bind);
    my $total = $self->connector->dbh->selectrow_array("select found_rows()");
    
    return ($result, $total);
}

sub add_email
{
    my ($self, $gid, $email) = @_;
    
    my $query = "insert ignore into mmail_emails (gid, email, send) values (?, ?, true)";
    
    $self->connector->dbh->do($query, undef, $gid, $email);
}

sub add_group
{
    my ($self, $group_name) = @_;
    
    my $res = $self->connector->dbh->do('insert into mmail_groups (name) values (?)', undef, $group_name);
    
    return 0 unless (defined $res && $res ne '0E0');
    return $self->connector->dbh->last_insert_id(undef, undef, 'mmail_groups', 'id');
}

sub list_email_groups
{
    my $self = shift;
    
    my $res = $self->connector->dbh->selectall_arrayref('select * from mmail_groups', { Slice => {}});
}

sub remove_group
{
    my ($self, $id) = @_;
    
    my $res = $self->connector->dbh->do('delete from mmail_groups where id = ?', undef, $id);
    return 0 unless (defined $res && $res ne '0E0');
    
    $res;
}

sub switch
{
    my ($self, $id) = @_;
    
    my $res = $self->connector->dbh->do('update mmail_emails set send = !send where id = ?', undef, $id);
    
    $res = 0 unless (defined $res && $res ne '0E0');
    
    return $res;
}

sub add_to_queue
{
    my ($self, $tid, $gids) = @_;
    
    my $ph = join (',', ('?') x scalar @$gids);
    my $mids = $self->connector->dbh->selectcol_arrayref("select id from mmail_emails where gid in ($ph) and send = 1", undef, @$gids);
    
    my $ph2 = join (',', ("( ?, ? )") x scalar @$mids);
    
    
    my @bind;
    for my $mid (@$mids)
    {
        push @bind, $tid, $mid;
    }
    
    my $res = $self->connector->dbh->do("insert into mmail_req(tid, mid)
                                         values $ph2", undef, @bind);
    
}

sub unscribe_email
{
    my ($self, $email) = @_;
    
    my $res = $self->connector->dbh->do("update mmail_emails set send = 0 where email = ?", undef, $email);
    
    $res = 0 unless (defined $res && $res ne '0E0');
    
    return $res;
}

1;