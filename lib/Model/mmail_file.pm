use common::sense;
package Model::mmail_file;
use DBIx::Custom::Model -base;

sub add_file
{
    my ($self, $path, $content_type, $name) = @_;
    
    my $res = $self->connector->dbh->do('insert into mmail_file (path, content_type, name)
                                         values (?, ?, ?)', undef, $path, $content_type, $name);
    
    
    return undef unless ($res || $res ne "0E0");
    return $self->connector->dbh->last_insert_id(undef, undef, 'mmail_file', 'id');
}

1;