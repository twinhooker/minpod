package Model::categories;

use Model -base;

has primary_key => sub {['id']};

sub list {
    my ($self, $count, $offset) = @_;

    $count   = 0 unless defined $count;
    $offset  = 0 unless defined $offset;
    $offset *= $count;

    my $query = "SELECT categories.id, categories.name, img_small, categories.img
                 FROM categories LEFT JOIN products ON products.cat_id = categories.id
                 group by categories.id order by coalesce (categories.sort_num, categories.id)";

    my @vars;
    if ( $count != 0 ) {
        $query .= " limit ?, ?";
        push @vars, $offset, $count;
    }

    my $result = $self->dbi->dbh->selectall_arrayref($query, {Slice => {}}, @vars);
    my $total  = $self->dbi->dbh->selectrow_array("select found_rows()");

    return ( $result, $total );
};

sub get_root {
    my $self = shift;
    my @root;

    my $query = "select node.*, (count(parent.name) - 1) as depth, images.thumb_url
                 from categories as parent,
                      categories as node
                 left join images on images.id = node.img_id
                 where node.left_key between parent.left_key and parent.right_key
                 group by node.id
                 order by node.left_key";
    my $sth = $self->dbi->dbh->prepare($query);
    $sth->execute;

    my %dispatcher = (
        0 => sub { push @root, $_[0]; },
        1 => sub {
            $root[-1]->{child_count}++;
            $root[-1]->{childs} = [] if !$root[-1]->{childs};
            push @{$root[-1]->{childs}}, $_[0];
        },
        2 => sub { }
    );
    
    while (my $node = $sth->fetchrow_hashref) {
        my $depth = $node->{depth};
        $dispatcher{$depth}->($node);
    }

    return \@root;
}

sub get_childs {
    my ($self, $parent_id) = @_;
    #warn Dumper($parent_id);
    #
    my $query = "select node.*, (count(parent.id) - 1) as depth, images.thumb_url
                 from categories as parent,
                      categories as sub_parent,
                      (
                          select node.id, (count(parent.id) - 1) AS depth
                          from categories as node,
                               categories as parent
                          where node.left_key between parent.left_key and parent.right_key
                            and node.id = ?
                          group by node.id
                          order by node.left_key
                      ) as sub_tree,
                      categories as node
                 left join images on node.img_id = images.id
                 where node.left_key between parent.left_key and parent.right_key
                   and node.left_key between sub_parent.left_key and sub_parent.right_key
                   and sub_parent.id = sub_tree.id
                 group by node.id
                 order by node.left_key";                  
    my $childs = $self->dbi->dbh->selectall_arrayref($query, {Slice => {}}, $parent_id);
    shift @$childs;    
    #use Data::Dumper; warn Dumper($childs);
    
    return $childs;
}

sub get_path {
    my ($self, $id) = @_;
    my @path;
    
    my $query = "select parent.name, parent.id
                 from categories as node,
                      categories as parent
                 where node.left_key between parent.left_key and parent.right_key
                   and node.id = ?
                 order by parent.left_key";

    my $sth = $self->dbi->dbh->prepare($query);
    $sth->execute($id);
    while (my @row = $sth->fetchrow_array) {
        push @path, {
            name => $row[0],
            url  => '/categories/' . $row[1]
        };
    }

    return \@path;
}


sub nested_tree {
    my ($self, $node_id) = @_;
    my $query = 
        'select node.id, node.name, node.meta_tags, node.description, (Count(parent.id) - (sub_tree.depth + 1)) as depth
         from categories as node,
              categories as parent,
              categories as sub_parent,
              (
                  select node.id, (count(parent.id) - 1) as depth
                  from categories as node,
                       categories as parent
                  where node.left_key between parent.left_key and parent.right_key
                    and node.id = ?
                  group by node.id
                  order by node.left_key
              ) as sub_tree
          where node.left_key between parent.left_key and parent.right_key
            and node.left_key between sub_parent.left_key and sub_parent.right_key
            and sub_parent.id = sub_tree.id
          group by node.id
          having depth <= 1
          order by node.left_key';
    
    my $result = $self->dbi->dbh->selectall_arrayref($query, undef, $node_id);
    my @tree   = map { tree_node(@$_) } @$result;
    
    return \@tree;
}

sub tree_tree {
    my $self = shift;
    my $query = "select node.id,
                        node.name, 
                        node.meta_tags,
                        node.description,
                        node.main_text,
                        (COUNT(parent.name) - 1) AS depth,
                        node.img_id,
                        images.img_url,
                        images.thumb_url,
                        node.left_key,
                        node.right_key
                 from categories as parent,
                      categories as node
                 left join images on images.id = node.img_id
                 where node.left_key between parent.left_key and parent.right_key
                 group by node.id
                 order by node.left_key";
        
    my $result = $self->dbi->dbh->selectall_arrayref($query, {Slice => {}});
    my ($nodes, $more) = build_level(0, $result);
    
    return $nodes;
}

sub build_level {
    my ($depth, $nodes) = @_;    
    my @level;
    while ( my $node = shift @$nodes ) {
        if ($node->{depth} == $depth) {
            push @level, tree_node(%$node);
        }
        elsif ($node->{depth} > $depth) {
            my $parent = \$level[-1];
            unshift @$nodes, $node;
            ($$parent->{children}, $nodes) = build_level($node->{depth}, $nodes);            
        }
        elsif ($node->{depth} < $depth) {
            unshift @$nodes, $node;            
            return (\@level, $nodes);
        }
    }
    return (\@level, $nodes);
}

sub tree_node {
    my %row = @_;

    my %node = (
        children => [],
        movable  => 1,
        a_attr   => {
            href => '/managment/gallery/' . $row{id},
            id   => "tree_" . $row{id}
        }
    );
    @node{qw(id
             text
             meta_tags
             description
             main_text
             depth
             img_id
             thumb_url
             img_url
             left_key
             right_key)}
        = @row{qw(id
                  name
                  meta_tags
                  description
                  main_text
                  depth
                  img_id
                  thumb_url
                  img_url
                  left_key
                  right_key)};
    return \%node;
}

#######################
#  Nested Tree sets 
#  ADD

sub add_cat {
    my ($self, $cat) = @_;
    my $parent_id = delete $cat->{parent_id};

    ($parent_id) ? $self->push_cat_to($parent_id, $cat)
    :              $self->push_cat_to_root($cat);
}
use List::MoreUtils qw(none);

sub move_cat {
    my ($self, $parent_id, $position, $cat_id) = @_;    
    $self->dbi->dbh->do('lock table categories write');
    
    my $neighbors = $self->_get_neighbors($parent_id);
    my $count     = $#$neighbors;
    my $new_group = none { $_->{id} == $cat_id } @$neighbors;
    my $new_left_key;
    if ($count == -1) {
        my ($parent_key) = $self->dbi->dbh->selectrow_array('select left_key from categories where id = ?', undef, $parent_id);
        $new_left_key = $parent_key + 1;
    }
    elsif ($position != $count && !$new_group) {
        $new_left_key = $neighbors->[$position]->{left_key};
    }
    elsif ($position == $count && !$new_group) {
        $new_left_key = $neighbors->[$position]->{right_key} + 1;
    }
    elsif ($position != $count + 1 && $new_group) {
        $new_left_key = $neighbors->[$position]->{left_key};
    }
    elsif ($position == $count + 1 && $new_group) {
        $new_left_key = $neighbors->[$position - 1]->{right_key} + 1;
    }

    my ($left_key, $right_key) = $self->dbi->dbh->selectrow_array('select left_key, right_key from categories where id = ?', undef, $cat_id);
    # calculate position adjustment variables
    my $width                  = $right_key - $left_key + 1;    
    my $distance               = $new_left_key - $left_key;
    my $tmp_pos                = $left_key;

    # backwards movement must account for new space
    if ($distance < 0) {
        $distance -= $width;
        $tmp_pos  += $width;
    }
    
    # create new space for subtree
    $self->dbi->dbh->do('update categories set left_key = left_key + ? where left_key >= ?', undef, $width, $new_left_key);
    $self->dbi->dbh->do('update categories set right_key = right_key + ? where right_key >= ?', undef, $width, $new_left_key);

    # move subtree into new space
    $self->dbi->dbh->do(
        'update categories set left_key = left_key + ?, right_key = right_key + ? where left_key >= ? and right_key < ? + ?',
        undef,
        $distance, $distance, $tmp_pos, $tmp_pos, $width
    );
    
    # remove old space vacated by subtree
    $self->dbi->dbh->do('update categories set left_key  = left_key  - ? where left_key  > ?', undef, $width, $right_key);
    $self->dbi->dbh->do('update categories set right_key = right_key - ? where right_key > ?', undef, $width, $right_key);
    
    $self->dbi->dbh->do('unlock tables');
}

sub _get_neighbors {
    my ($self, $parent_id) = @_;

    my ($query, @bind);    
    if ($parent_id eq '#') {
        $query =
            'select node.id,(COUNT(parent.name) - 1) AS depth, node.right_key, node.left_key
             from categories as parent,
                  categories as node
             left join images on images.id = node.img_id
             where node.left_key between parent.left_key and parent.right_key
             group by node.id
             having depth = 0
             order by node.left_key';
    }
    else {
        $query =
            'select node.id, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth, node.right_key, node.left_key
             from categories as node,
                  categories as parent,
                  categories as sub_parent,
                  (
                      select node.id, (COUNT(parent.name) - 1) AS depth
                      from categories as node,
                           categories as parent
                      where node.left_key between parent.left_key and parent.right_key
                      and node.id = ?
                      group by node.id
                      order by node.left_key
                   ) as sub_tree
             where node.left_key between parent.left_key and parent.right_key
             and node.left_key between sub_parent.left_key and sub_parent.right_key
             and sub_parent.id = sub_tree.id
             group by node.id
             having depth = 1
             order by node.left_key';
        push @bind, $parent_id;
    }
    $self->dbi->dbh->do('unlock tables;');
    
    my $neighbors = $self->dbi->dbh->selectall_arrayref($query, {Slice => {}}, @bind);
    
    return $neighbors;
}

sub push_cat_to_root {
    my ($self, $category) = @_;
    
    my @bind = @{$category}{qw(name meta_tags description img_id)};

    $self->dbi->dbh->do('lock table categories write');
    $self->dbi->dbh->do('select right_key into @neighborRight from categories order by left_key desc limit 1');
    $self->dbi->dbh->do(
        'insert into categories (name, meta_tags, description, img_id, left_key, right_key) values (?,?,?,?, @neighborRight + 1, @neighborRight + 2)',
        undef, @bind
    );
    my $cat_id = $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
    $self->dbi->dbh->do('unlock tables');
    
    return $cat_id;
}

sub add_left_neighbor_to {
    my ($self, $neighbor_id, $category) = @_;

    my @bind = @{$category}{qw(name meta_tags description img_id)};
    $self->do('lock table categories write');
    $self->do('select left_key into @left_key from categories where id = ?', undef, $neighbor_id);
    $self->do('update categories set left_key = left_key + 2 where left_key > @left_key');
    $self->do('update categories set right_key = right_key + 2 where right_key > @left_key');
    $self->do(
        'insert into categories (name, meta_tags, description, img_id, left_key) values (?,?,?,?, @left_key, @left_key + 1',
        undef, @bind
    );
    my $cat_id = $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
    $self->dbi->dbh->do('unlock tables');
    return $cat_id;    
}

sub add_right_neighbor_to {
    my ($self, $neighbor_id, $category) = @_;
    my @bind = @{$category}{qw(name meta_tags description img_id)};
    $self->do('lock table categories write');
    $self->dbi->dbh->do('select right_key into @right_key from categories where id = ?', undef, $neighbor_id);
    $self->dbi->dbh->do('update categories set right_key = right_key + 2 where right_key > @right_key');
    $self->dbi->dbh->do('update categories set left_key  = left_key  + 2 where left_key  > @right_key');
    $self->dbi->dbh->do(
        'insert into categories (name, meta_tags, description, img_id, left_key, right_key)
         values (?,?,?,?, @right_key + 1, @right_key + 2)',
        undef, @bind
    );
    my $cat_id = $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
    $self->dbi->dbh->do('unlock tables');
    return $cat_id;
}

    
sub push_cat_to {
    my ($self, $parent_id, $category) = @_;

    my @bind = @{$category}{qw(name meta_tags description img_id)}; 
    $self->dbi->dbh->do('lock table categories write');
    $self->dbi->dbh->do('select right_key into @parent_right from categories where id = ?', undef, $parent_id);
    $self->dbi->dbh->do('update categories set right_key = right_key + 2 where right_key > @parent_right');
    $self->dbi->dbh->do('update categories set left_key  = left_key  + 2 where left_key  > @parent_right');
    $self->dbi->dbh->do('update categories set right_key = right_key + 2 where id = ?', undef, $parent_id);

    $self->dbi->dbh->do(
        'insert into categories (name, meta_tags, description, img_id, left_key, right_key)
         values (?,?,?,?, @parent_right, @parent_right + 1)',
        undef, @bind
    );
    my $cat_id = $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
    $self->dbi->dbh->do('unlock tables');
    return $cat_id;
}
# END ADD

sub delete_node {
    my ($self, $node_id) = @_;
    $self->dbi->dbh->do('lock table categories write');
    $self->dbi->dbh->do('select @myLeft := left_key, @myRight := right_key, @myWidth := right_key - left_key + 1 from categories where id = ?', undef, $node_id);
    $self->dbi->dbh->do('delete from categories where left_key between @myLeft and @myRight;');
    $self->dbi->dbh->do('update categories set right_key = right_key - @myWidth where right_key > @myRight');
    $self->dbi->dbh->do('update categories set left_key  = left_key - @myWidth where left_key > @myRight');
    $self->dbi->dbh->do('unlock tables');

    return 1;
}


sub get_tree_as_list {
    my ($self) = @_;

    $self->dbi->dbh->selectall_arrayref(
        "select node.id, concat( repeat('&nbsp;&nbsp', count(parent.name) - 1), node.name) as name
         from categories as node,
              categories as parent
         where node.left_key between parent.left_key and parent.right_key
         group by node.id
         order by node.left_key",
        { Slice => {} }
    );
}

sub js_tree {
    my $self = shift;

    my $list = $self->dbi->dbh->selectall_arrayref(
        "SELECT c.id, c.name, c.meta_tags, c.description, i.id as img_id, i.img_url, i.thumb_url, c.parent_id
         FROM categories c LEFT JOIN images i ON c.img_id = i.id
         group by c.id order by coalesce (c.sort_num, c.id)",
        { Slice => {} }
    );

    return $list;
}

sub get {
    my ($self, $id) = @_;
    
    my $result = $self->select('where' => {'id' => $id})->one;
    
    scalar keys %{$result} ? $result : undef;
}

sub add
{
    my ($self, $params) = @_;
  
    my $cols = ['name', 'description', 'meta_tags'];
    
    my $params_checked;
    
    foreach my $key (@$cols)
    {
        next unless (exists ($params->{$key}));
        $params_checked->{$key} = $params->{$key};
    }
    
    my $result = $self->insert($params_checked);
    
    return undef unless ($result == 1);
    
    $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
}

sub up
{
    my ($self, $id, $params) = @_;
    
    my $cols = ['name', 'description', 'meta_tags'];
    
    my $params_checked;
    $params_checked->{$_} = $params->{$_} foreach (@$cols);
    
    my $result = $self->update($params_checked, 'where' => { 'id' => $id});
}

sub switch_cats
{
    my ($self, $ids) = @_;
    
    my $res = $self->dbi->dbh->do("update categories
                             set sort_num = case
                                            when id = ? then ?
                                            when id = ? then ?
                                            end
                                where id in(?,?)", undef,
                                $ids->[0]->[0], $ids->[1]->[1],
                                $ids->[1]->[0], $ids->[0]->[1],
                                $ids->[0]->[0], $ids->[1]->[0]);
    
    return undef unless ($res == 2);
    $res;
}

sub weight_up
{
    my ($self, $id) = @_;
    
    my $rows = $self->dbi->dbh->selectall_arrayref("select id, coalesce(sort_num, id)
                                            from categories
                                            where coalesce(sort_num,id) <= (select coalesce(sort_num,id) from categories where id = ?)
                                            order by coalesce(sort_num, id) desc limit 2",
                                          undef, $id);
    
    $self->switch_cats($rows);
}

sub weight_down
{
    my ($self, $id) = @_;
    
    my $rows = $self->dbi->dbh->selectall_arrayref("select id, coalesce(sort_num, id)
                                            from categories
                                            where coalesce(sort_num,id) >= (select coalesce(sort_num,id) from categories where id = ?)
                                            order by coalesce(sort_num, id) asc limit 2",
                                            undef, $id);
    
    $self->switch_cats($rows);
}

sub rem
{
    my ($self, $id) = @_;
    
    my $res = $self->delete(where => { id => $id });
    
    return ($res == 1) ? 1 : undef;
}

sub search_phrase
{
    
    my ($self, $search_str) = @_;
    
    my $query = 'select i.thumb_url, c.*
                 from categories as c
                 left outer join images as i
                 on i.id = c.img_id
                 where c.name like ?';
                 
    my $res = $self->dbi->dbh->selectall_hashref($query, 'id', {Slice => {}}, "%$search_str%");
    
    my @words = split '\s+', $search_str;
    
    my $sth = $self->dbi->dbh->prepare($query);
    for my $word (@words)
    {
        $word =~ s/\W+//g;
        $sth->execute("%$word%");
        while (my $cat = $sth->fetchrow_hashref) {
           my $id = $cat->{id};
           $res->{$id} = $cat unless ($res->{$id});
           $res->{$id}->{search_weight} = 0 unless $res->{$id}->{search_weight};
           $res->{$id}->{search_weight}++;
        }
    }
    return $res;
}

sub set_image
{
    my ($self, $cat_id, $p_id) = @_;

    my $res = $self->dbi->dbh->do('update categories
                                        set img = (select img_small from products where id = ?)
                                        where id = ?',
                                        undef,
                                        $p_id, $cat_id);
    return undef unless ($res == 1);
    return $res;
}

1;
