package Model::users;
use JSON::XS qw( encode_json decode_json );
use DBIx::Custom::Model -base;

has primary_key => sub { ['id'] };

sub get_user {
    my ( $self, $uid ) = @_;

    my $res = $self->select( where => { id => $uid } )->one;

    scalar keys %{$res} ? $res : undef;
}

sub get_order_user {
    my ( $self, $order_id ) = @_;

    my $res = $self->connector->dbh->selectall_arrayref(
        "SELECT u.contact_name,
                u.email,
                u.phone,
                u.address
        FROM orders as o
        JOIN users as u ON o.user_id = u.id
        WHERE o.id = ?",
        { Slice => {} },
        $order_id
    )->[0];
    if ( $res && $res->{address} ) {
        my $json = JSON::XS->new->utf8( 0 );
        $res->{address} = $json->decode( $res->{address} );
    }

    return $res;
}

sub auth {
    my ( $self, $email, $pwd ) = @_;

    my $res = $self->select(
        where => {
            email    => $email,
            password => $pwd
        }
    )->one;

    scalar keys %{$res} ? $res->{id} : undef;
}

sub list {
    my $self = shift;

    my $res = $self->select()->fetch_hash_all;
}

sub create_user {
    my ( $self, $params ) = @_;

    my @cols = qw( email contact_name phone );
    my %user_info;

    @user_info{ @cols }  = @{ $params }{ @cols };
    $user_info{password} = gen_rand_str(5);
    my %address;
    @address{ qw( city house_number house_letter flat street shipment_self ) }
        = @{ $params }{ qw( city house_number house_letter flat street shipment_self ) };
    $user_info{address}  = encode_json( \%address );
    my $result = $self->insert( \%user_info );
    return undef unless ($result == 1);

    my $id = $self->dbi->dbh->last_insert_id( undef, undef, 'users', 'id' );

    return $id;
}

sub gen_rand_str {
    my $str_len = shift;

    my @chars = ('a'..'z','A'..'Z', '1'..'9');

    my $random_str;

    for (1 .. $str_len) {
        $random_str .= $chars[rand @chars];
    }

    return $random_str;
}

sub rem {
    my ($self, $id) = @_;

    my $res = $self->delete(where => {id => $id});

    return undef unless $res == 1;

    1;
}

1;
