package Model::tags;
use DBIx::Custom::Model -base;

has primary_key => sub { [ 'id' ] };

sub gallery_list {
    my $self = shift;

    my $query = 'select t.id, t.name, t.img_id
                 from tags t
                 where show_on_main is true
                 order by coalesce(t.id, t.sort_num)';
    my $list = $self->dbi->dbh->selectall_arrayref( $query, {Slice => {}} );
    my @list
        = map {
            my ( $thumb_url ) = $self->dbi->dbh->selectrow_array(
                'select thumb_url from images where id = ?',
                undef, $_->{img_id}
            );
            $_->{thumb_url} = $thumb_url;
        } @$list;

    return $list;
}

sub list {
    my ( $self ) = @_;

    my $query = <<EOQ;
select t.*, i.thumb_url as gallery_img_url, i2.thumb_url as product_img_url
from tags t
left outer join images i on i.id = t.img_id
left outer join images i2 on i2.id = t.product_img_id
EOQ
    my $list = $self->dbi->dbh->selectall_arrayref( $query , { Slice => {} } );
    
    return $list;
}

sub get {
    my ( $self, $id ) = @_;

    my $query = <<EOQ;
select t.*, i.thumb_url as gallery_img_url, i2.thumb_url as product_img_url
from tags t
left outer join images i on i.id = t.img_id
left outer join images i2 on i2.id = t.product_img_id
where t.id = ?
EOQ
    my $tag = $self->dbi->dbh->selectrow_hashref( $query, undef, $id );

    return $tag
}

sub products_for {
    my ($self, $tag_id) = @_;

    my $result = $self->dbi->dbh->selectall_arrayref(
        "select p.id,      p.name,        p.code, c.name as category_name,
                p.packing, p.price,       p.material, p.new_price,
                p.height,  p.description, p.in_stock, i.thumb_url, i.img_url
        from products as p
        left outer join images as i on i.id = p.img_id
        join tagged_products tp on tp.product_id = p.id
        join categories c on c.id = p.cat_id
        where tp.tag_id = ?
        order by c.id, p.id",
        { Slice => {} }, $tag_id
    );

    return $result;
}

sub update_tags_for {
    my ($self, $product_id, $tags) = @_;

    my @tag_ids;
    push @tag_ids, (ref $tags eq 'ARRAY') ? @$tags : $tags;
    $self->dbi->dbh->do('delete from tagged_products where product_id = ?', undef, $product_id);

    my $phs  = join (',', ('(?, ?)') x scalar(@tag_ids));
    my @bind = map { $product_id, $_ } @tag_ids;
    $self->dbi->dbh->do("insert into tagged_products (product_id, tag_id) values ${phs}", undef, @bind);

    return;
}

    

sub tags_update_for {
    my ($self, $product_id, $tags) = @_;

    
    my @tags;
    push @tags, (ref $tags eq 'ARRAY') ? @$tags : $tags;
    my %new_tags = map { $_ => 1 } @tags;
    
    my $old_tags = $self->dbi->dbh->selectall_arrayref(
        'select tag_id, id from tagged_products where product_id = ?',
        undef,
        $product_id
    );
    my %old_tags = map { $_->[0] => $_->[1] } @$old_tags;
    
    for my $old_id ( keys %old_tags ) {
        $self->dbi->dbh->do('delete from tagged_products where id = ?', undef, $old_tags{$old_id})
            if (! exists $new_tags{$old_id} );
    }

    for my $new_id ( keys %new_tags ) {
        $self->dbi->dbh->do('insert into tagged_products (product_id, tag_id)values (?,?)', undef, $product_id, $new_id)
            if (! exists $old_tags{$new_id} );
    }
    
}


sub weight_up {
    my ($self, $id) = @_;

    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from tags
        where coalesce(sort_num,id) <= (select coalesce(sort_num,id) from tags where id = ?)
        order by coalesce(sort_num, id) desc limit 2",
        undef,
        $id
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_position($rows, 'tags');
}

sub weight_down {
    my ( $self, $id ) = @_;
    
    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from tags
        where coalesce(sort_num,id) >= (select coalesce(sort_num,id) from tags where id = ?)
        order by coalesce(sort_num, id) asc limit 2",
        undef,
        $id
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_position($rows, 'tags');
}

sub tagged_product_weight_up {
    my ( $self, $id ) = @_;

    my @vars = ($id, $id);    
    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from tagged_products
        where coalesce(sort_num,id) <= (select coalesce(sort_num,id) from tagged_products where id = ?)
        and tag_id = (select tag_id from tagged_products where id = ?)
        order by coalesce(sort_num, id) desc limit 2",
        undef,
        @vars
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_position($rows, 'tagged_products');
}

sub tagged_product_weight_down {
    my ($self, $id) = @_;

    my @vars = ($id, $id);
    
    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from tagged_products
        where coalesce(sort_num,id) >= (select coalesce(sort_num,id) from tagged_products where id = ?)
        and tag_id = (select tag_id from tagged_products where id = ?)
        order by coalesce(sort_num, id) asc limit 2",
        undef,
        @vars
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_position($rows, 'tagged_products');
}


sub switch_position {
    my ($self, $ids, $table) = @_;
    my $query = "update $table set
                     sort_num = case
                         when id = ? then ?
                         when id = ? then ?
                     end
                 where id in(?,?)";
    
    my $res = $self->dbi->dbh->do(
        $query, undef,
        $ids->[0]->[0], $ids->[1]->[1],
        $ids->[1]->[0], $ids->[0]->[1],
        $ids->[0]->[0], $ids->[1]->[0]
    );    
    return undef unless ($res == 2);
    $res;
}


1;
