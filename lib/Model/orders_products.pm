package Model::orders_products;
use DBIx::Custom::Model -base;

sub add {
    my ( $self, $order_id, $cart ) = @_;

    my $count;
    for my $item_id ( keys %$cart ) {
        my %params = (
            order_id   => $order_id,
            product_id => $item_id,
            quantity   => $cart->{ $item_id }
        );
        $count++ if ( $self->insert( \%params ) );
    }

    return $count;
}

1;
