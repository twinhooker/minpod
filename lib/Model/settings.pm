package Model::settings;
use DBIx::Custom::Model -base;

sub get_main_metas {
    my $self = shift;

    my $res = $self->dbi->dbh->selectall_hashref(
        'select name, value from settings
         where name = "main_meta_keywords" OR name = "main_meta_description"',
        'name',
        { Slice => {} }
    );

    return $res;
}

sub save_settings {
    my ($self, $params) = @_;

    my $err = 0;
    while (my ($name, $value) = each (%$params)) {
        my $res = $self->dbi->dbh->do(
            'insert into settings (name, value)
             values (?, ?)
             on duplicate key update value = ?',
            undef,
            $name, $value, $value
        );

        $err = 1 unless (defined $res && $res ne '0E0');
    }

    return ($err) ? undef : 1;
}

sub get_unscribe_text {
    my $self = shift;

    my ($res) = $self->dbi->dbh->selectrow_array(
        'select value from settings where name = "unscribe_text"'
    );

    return $res;
}

sub get_value_for {
    my ($self, $param_name) = @_;

    my ($res) = $self->dbi->dbh->selectrow_array(
        'select value from settings where name = ?',
        undef,
        $param_name
    );

    return $res;
}

1;
