CREATE PROCEDURE MoveSubtree @my_root CHAR(2), @new_parent CHAR(2)
AS
DECLARE
  @origin_lft INT,
  @origin_rgt INT,
  @new_parent_rgt INT

    SELECT @new_parent_rgt = rgt
    FROM categories
    WHERE node = @new_parent;

    SELECT @origin_lft = lft, @origin_rgt = rgt
    FROM Tree
    WHERE node = @my_root;

    IF @new_parent_rgt < @origin_lft
    BEGIN
        UPDATE Tree SET lft = lft + CASE
            WHEN lft BETWEEN @origin_lft AND @origin_rgt THEN
                @new_parent_rgt - @origin_lft
            WHEN lft BETWEEN @new_parent_rgt AND @origin_lft - 1 THEN
                @origin_rgt - @origin_lft + 1
            ELSE 0 END,
        rgt = rgt + CASE
            WHEN rgt BETWEEN @origin_lft AND @origin_rgt THEN
                @new_parent_rgt - @origin_lft
            WHEN rgt BETWEEN @new_parent_rgt AND @origin_lft - 1 THEN
                @origin_rgt - @origin_lft + 1
            ELSE 0 END
        WHERE lft BETWEEN @new_parent_rgt AND @origin_rgt
        OR rgt BETWEEN @new_parent_rgt AND @origin_rgt;
    END
    ELSE IF @new_parent_rgt > @origin_rgt
    BEGIN
        UPDATE Tree SET
        lft = lft + CASE
            WHEN lft BETWEEN @origin_lft AND @origin_rgt THEN
                @new_parent_rgt - @origin_rgt - 1
            WHEN lft BETWEEN @origin_rgt + 1 AND @new_parent_rgt - 1 THEN
                @origin_lft - @origin_rgt - 1
            ELSE 0 END,
        rgt = rgt + CASE
            WHEN rgt BETWEEN @origin_lft AND @origin_rgt THEN
                @new_parent_rgt - @origin_rgt - 1
            WHEN rgt BETWEEN @origin_rgt + 1 AND @new_parent_rgt - 1 THEN
                @origin_lft - @origin_rgt - 1
            ELSE 0 END
        WHERE lft BETWEEN @origin_lft AND @new_parent_rgt
        OR rgt BETWEEN @origin_lft AND @new_parent_rgt;
    END
ELSE BEGIN
  PRINT 'Cannot move a subtree to itself, infinite recursion';
  RETURN;
END
