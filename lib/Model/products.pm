package Model::products;
use Model -base;
use POSIX qw/ceil floor/;
use IO::String;
use utf8;
use URI;
use Excel::Writer::XLSX;
use Encode qw(encode_utf8);

use constant MINISTERSTVO_URL => 'http://ministerstvo-podarkov.com';

has primary_key => sub { [ 'id' ] };

sub add {
    my ($self, $data) = @_;
    my @valid_cols = qw(name code packing img_id img_small cat_id price material height description);

    my $tags = delete $data->{tags};
    my %valid_data = map  { ($_ => $data->{$_}) }
                     grep { $data->{$_}         } @valid_cols;

    $self->insert(\%valid_data);
    my $id = $self->dbi->dbh->last_insert_id(undef, undef, 'products', 'id');
    $self->update_tags_for($id, $tags);

    my $product = $self->get($id);
    return $product;
}

sub update_tags_for {
    my ($self, $product_id, $tags) =  @_;

    my @tag_ids;
    push @tag_ids, (ref $tags eq 'ARRAY') ? @$tags : $tags;
    $self->dbi->dbh->do('delete from tagged_products where product_id = ?', undef, $product_id);

    my $phs  = join (',', ('(?, ?)') x scalar(@tag_ids));
    my @bind = map { $product_id, $_ } @tag_ids;
    $self->dbi->dbh->do("insert into tagged_products (product_id, tag_id) values ${phs}", undef, @bind);

    return;
}

sub up {
    my ($self, $params, $id) = @_;

    my $cols = [ 'name', 'code', 'packing', 'img', 'img_small', 'cat_id' , 'price', 'material', 'height', 'description' ];

    my $params_checked;
    foreach my $key (@$cols) {
        if (exists ($params->{$key})) {
            $params_checked->{$key} = $params->{$key};
        }
    }
    my $result;
    if (defined $id) {
        $result = $self->update($params_checked, where => { id => $id });
        return $result;
    }
    else {
        $result = $self->insert($params_checked);
        return $self->dbi->dbh->last_insert_id(undef, undef, 'categories', 'id');
    }    
}

sub order {
    my ($self, $user, $cart) =@_;
    my $order = {user_id => $user->{id}, progress => 0};

    $self->insert(table => 'orders', $order);
    my $order_id = $self->dbi->dbh->last_insert_id(undef, undef, 'orders', 'id');

    return undef unless ($order_id);
    for my $item (keys %$cart) {
        my $params = {order_id => $order_id, product_id => $item, quantity => $cart->{$item}};
        $self->insert( table => 'order_products', param => $params);
    }

    1;
}

sub cart_list {
    my ( $self, @ids ) = @_;

    return undef unless (@ids);
    my $ph = join (',', ('?') x scalar (@ids));
    $ph = '(' . $ph .')';

    $self->dbi->dbh->selectall_arrayref(
        'select p.*, i.img_url, i.thumb_url
         from products p
         left outer join images as i on i.id = p.img_id
         where p.id in ' . $ph,
        { Slice => {} },
        @ids
    );

}

sub export {
    my ( $self, %params ) = @_;

    my @export_fields   = qw( code name category_name img_url thumb_url price packing material height );
    my @selected_fields = ();

    my %export_field_names = (
        name          => 'Наименование',
        category_name => 'Категория',
        code          => 'Код',
        img_url       => 'Ссылка на оригинальное изображение',
        thumb_url     => 'Ссылка на изображение',
        price         => 'Цена',
        packing       => 'Упаковка',
        material      => 'Материал',
        height        => 'Высота',
    );

    for my $name ( @export_fields ) {
        push @selected_fields, $name if ( exists $params{fields}{ $name } );
    }
    my @products;
    if ( $params{cats} and @{ $params{cats} } ) {
        for my $cat_id ( @{$params{cats}} ) {
            my ( $cat_products ) = $self->dbi->model('products')->get_for_export( $cat_id );

            push @products, map {
                my $product = $_;
                my @product_info = @{ $product }{ @selected_fields };
                \@product_info;
            } @$cat_products;
        }
    }
    if ( $params{tags} and @{ $params{tags} } ) {
        my $uri = URI->new( MINISTERSTVO_URL() );
        for my $tag_id ( @{ $params{tags} } ) {
            my $tag_products = $self->dbi->model('tags')->products_for( $tag_id );
            push @products, map {
                my $product = $_;

                $uri->path( $product->{thumb_url} );
                $product->{thumb_url} = "$uri";

                $uri->path( $product->{img_url} );
                $product->{img_url} = "$uri";
                
                my @product_info = @{ $product }{ @selected_fields };
                \@product_info;
            } @$tag_products;
        }
    }
    my %export_params = ( head => [@export_field_names{ @selected_fields } ], products => \@products );

    return ( $params{type} eq 'csv'  ) ? $self->export_csv( %export_params )
         : ( $params{type} eq 'xlsx' ) ? $self->export_xls( %export_params )
         :                               '';
}

sub export_csv {
    my ( $self, %params ) = @_;

    my $csv = Text::CSV->new({ binary => 1, sep => ';' });
    my $tmp = IO::String->new;
    $csv->say( $tmp , $params{head} );
    $csv->say( $tmp, $_ ) for ( @{$params{products}} );

    return encode_utf8(${$tmp->string_ref});
}

sub export_xls {
    my ( $self, %params ) = @_;

    my $tmp       = IO::String->new;
    my $workbook  = Excel::Writer::XLSX->new( $tmp );
    my $worksheet = $workbook->add_worksheet();

    my @data = ( $params{head}, @{ $params{products} } );

    for ( my $row_num = 0; $row_num < @data; $row_num++ ) {
        my $row = $data[ $row_num ];

        for ( my $col_num = 0; $col_num < @$row; $col_num++ ) {
            $worksheet->write( $row_num, $col_num, $row->[ $col_num ] );
        }
    }
    $workbook->close;

    return ${$tmp->string_ref};
}

sub get_for_export {
    my ( $self, $cat_id ) = @_;

    my $uri   = URI->new( MINISTERSTVO_URL() );
    my $query = <<EOQ;
select p.name, p.code, c.name as category_name, i.thumb_url, i.img_url, coalesce( p.new_price, p.price ), p.packing, p.price, p.material, p.height
from products p
join categories c on c.id = p.cat_id
join images     i on i.id = p.img_id
where p.cat_id = ? and p.in_stock = 1
order by c.id, p.id
EOQ
    my $result = $self->dbi->dbh->selectall_arrayref( $query, { Slice => {} }, $cat_id );

    map {
        $uri->path( $_->{thumb_url} );
        $_->{thumb_url} = "$uri";

        $uri->path( $_->{img_url} );
        $_->{img_url} = "$uri";
    } @$result;

    return $result;
}

sub get_cat {
    my ($self, $cat_id, $count, $offset, $order) = @_;

    my @vars;
    push @vars ,$cat_id;
    my $limit_str = ''; 
    if (defined $count) {
        $count = 10 if ($count < 10);
        $offset = 0 unless defined $offset;
        push @vars, $offset, $count;
        $limit_str = 'limit ?, ?';
    }
    my $in_stock = ($count) ? "and p.in_stock = 1" : "";

    $order ||= 'desc';

    my $result = $self->dbi->dbh->selectall_arrayref(
        "select sql_calc_found_rows
             p.id, coalesce(p.sort_num, p.id) as sort_order, p.name,     p.code,
             p.packing,     p.img,        p.img_small,
             p.price,       p.new_price,  p.material,  p.height,
             p.description, np.id as new, p.in_stock,  i.thumb_url, i.img_url
        from products as p
        left outer join new_products as np on np.product_id = p.id
        left outer join images as i on i.id = p.img_id
        where p.cat_id = ?
        order by p.sort_num asc, p.id desc
        $limit_str",
        { Slice => {} },
        @vars
    );

    map {
        my $tags = $self->dbi->dbh->selectall_arrayref(
            "select t.*, i.img_url as product_img_url from tags t
             join tagged_products tp on tp.tag_id = t.id 
             left outer join images i on i.id = t.product_img_id
             where tp.product_id = ?",
            { Slice => {} },
            $_->{id}
        );
        $_->{tags} = $tags;
    } @$result;

    my $total = $self->dbi->dbh->selectrow_array('select found_rows()');

    return ($result, $total);
}

sub get {
    my ($self, $id) = @_;

    #my $result = $self->select('where' => {'id' => $id})->one;
    my $result = $self->dbi->dbh->selectrow_hashref(
        'select p.id,
                p.name,
                p.cat_id,
                p.code,
                p.packing,
                p.meta_tags,
                p.price,
                p.new_price,
                p.material,
                p.height,
                p.description,
                p.sort_num,
                p.in_stock,
                p.img_id,
                i.thumb_url as img_small,
                i.img_url as img
        from products p
        left outer join images i on i.id = p.img_id
        where p.id = ?',
        undef, $id
    );

    return undef unless ($result);

    my $tags_selected = $self->dbi->dbh->selectall_arrayref(
        'select t.id, tp.product_id
         from tags t
         join  tagged_products tp on tp.tag_id = t.id
         where tp.product_id = ?',
        { Slice => {} }, $id
    );
    $result->{tags} = $tags_selected;

    return $result;
}

sub rem {
    my ($self, $id) = @_;

    my $result = $self->delete(where => { id => $id });

    return $result;
}

sub switch_products {
    my ($self, $ids, $table) = @_;
    my $query = "update $table set
                     sort_num = case
                         when id = ? then ?
                         when id = ? then ?
                     end
                 where id in(?,?)";

    my $res = $self->dbi->dbh->do(
        $query, undef,
        $ids->[0]->[0], $ids->[1]->[1],
        $ids->[1]->[0], $ids->[0]->[1],
        $ids->[0]->[0], $ids->[1]->[0]
    );
    return undef unless ($res == 2);
    $res;
}

sub weight_up {
    my ($self, $id, $new_flag) = @_;

    my @vars = ($id);

    my ($table, $where, $id_field)
        = ( $new_flag ) ? ('new_products', '', 'product_id')
        :                 ('products', 'and cat_id = (select cat_id from products where id = ?)', 'id');

    push @vars, $id if ( $table  eq 'products' );
    
    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from $table
        where coalesce(sort_num,id) >= (select coalesce(sort_num,id) from $table where $id_field = ?)
        $where
        order by coalesce(sort_num, id) asc limit 2",
        undef,
        @vars
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_products($rows, $table);
}

sub weight_down {
    my ($self, $id, $new_flag) = @_;

    my @vars = ($id);
    my ($table, $where, $id_field)
        = ( $new_flag ) ? ('new_products', '', 'product_id')
        :                 ('products', 'and cat_id = (select cat_id from products where id = ?)', 'id');

    push @vars, $id if ( $table eq 'products' );  
    
    my $rows = $self->dbi->dbh->selectall_arrayref(
        "select id, coalesce(sort_num, id)
        from $table
        where coalesce(sort_num,id) <= (select coalesce(sort_num,id) from $table where $id_field = ?)
        $where
        order by coalesce(sort_num, id) desc limit 2",
        undef,
        @vars
    );
    
    return undef if (scalar @$rows < 2);
    $self->switch_products($rows, $table);
}

sub suggestion {
    my ($self, $word) = @_;
    
    # search cat name
    my $query_word =  "$word*";
    my $res;
    
    my $query = "select distinct name from categories where match (name) against (? in boolean mode)";
    my $cats  = $self->dbi->dbh->selectcol_arrayref($query, undef, $query_word);

    if (scalar @$cats > 0) {
        push @$res, $cats->[0];
        if (scalar @$cats > 1) {
            push @$res, $cats->[1];
        }
    }
    
    
    $query       = "select distinct name from products where match (name) against (? in boolean mode)";
    my $products = $self->dbi->dbh->selectcol_arrayref($query, undef, $query_word);
    push @$res, @$products if (scalar @$products);
    
    $query    = "select distinct code from products where match (code) against (? in boolean mode)";
    $products =  $self->dbi->dbh->selectcol_arrayref($query, undef, $query_word);
    map {push @$res, "Код: " . $_} @$products;
    
    return $res;
}

sub search_phrase {
    my ($self, $search_str) = @_;
    
    my $query = 'select distinct c.name as cat_name, i.thumb_url, i.img_url, p.*
                 from products as p
                 join categories as c
                 join images as i
                 on c.id = p.cat_id and
                    i.id = p.img_id
                 where p.name like ?';
                 
    my $res = $self->dbi->dbh->selectall_hashref(
        $query, 'id', {Slice => {}}, "%$search_str%"
    );
    
    my @words = split '\s+', $search_str;
    
    my $sth = $self->dbi->dbh->prepare(
        'select distinct c.name,
                         i.thumb_url,
                         i.img_url,
                         p.*
         from categories as c
         join products as p
         join images   as i
         on c.id = p.cat_id and
            i.id = p.img_id
         where p.name like ? or p.code like ?'
    );
    for my $word (@words) {
        $word =~ s/\W+//g;
        $sth->execute('%' . $word . '%', '%' . $word . '%');
        while (my $product = $sth->fetchrow_hashref) {
            my $id = $product->{id};
            $res->{$id} = $product unless ($res->{$id});
           
            $res->{$id}->{search_weight} = 0 unless $res->{$id}->{search_weight};
            
            $product->{search_weight}++;
        }
    }
    
    return $res;
}

sub search_word {
    my ($self, $search_str) = @_;
    $search_str =~ s/\W+//g;
    
    my $query = 'select distinct c.name, p.* from categories as c join products as p on c.id = p.cat_id where p.name like ?';
    
    $self->dbi->dbh->selectall_hashref($query, 'id', {Slice => {}}, "%$search_str%");
}

sub get_pos {
    my ($self, $id, $cat_id) = @_;
    
    my $sth = $self->dbi->dbh->prepare('select id
                                              from products
                                              where cat_id = ?
                                              order by coalesce(sort_num, id)');
    $sth->execute($cat_id);
    
    my $i = 0;
    my $row;
    while ($row = $sth->fetchrow_array) {
        $i++;
        last if ($row == $id);
    }
    $sth->finish;
    
    return floor($i / 10);
}

sub toggle {
    my ($self, $id) = @_;
    
    my $res = $self->dbi->dbh->do('update products set in_stock = !in_stock where id = ?', undef, $id);
    
    $res = 0 unless (defined $res && $res ne '0E0');
    
    return $res;
}

sub add_to_new {
    my ($self, $id) = @_;
    
    my $query = 'insert into new_products (product_id) values (?)';

    return $self->dbi->dbh->do($query, undef, $id);
}

sub remove_from_new {
    my ($self, $id) = @_;

    my $query = 'delete from new_products where product_id = ?';

    my $res = $self->dbi->dbh->do($query, undef, $id);
    
    return ( ($res eq '0E0') || (!defined($res)) ) ? 0 : $res; 
}

sub list_new {
    my ($self, $count, $offset) = @_;

    my @vars;
    my $limit_str = '';

    # get limit
    if (defined $count) {
        $count = 10 if ($count < 10);
        $offset = 0 unless defined $offset;
        push @vars, $offset, $count;
        $limit_str = 'limit ?, ?';
    }

    # prepare query
    my $query = 'select sql_calc_found_rows
                     p.id,          p.name,     p.code,
                     p.packing,     p.img,      p.img_small,
                     p.price,       p.material, p.height,
                     p.description
                 from new_products as np
                 join products as p on p.id = np.product_id
                 order by coalesce(np.sort_num, np.id) desc ';

    $query .= $limit_str;

    my $result = $self->dbi->dbh->selectall_arrayref(
        $query,
        {Slice => {}},
        @vars
    );

    my $total = $self->dbi->dbh->selectrow_array( 'select found_rows()' );

    return ($result, $total);
}

sub toggle_new {
    my ($self, $id) = @_;

    my $found = $self->dbi->dbh->selectall_arrayref(
        'select id from new_products where product_id = ?',
        undef,
        $id
    );
    my $res = ( scalar @$found ) ? $self->remove_from_new($id)
            :                      $self->add_to_new($id);

    $res = 0 unless (defined $res && $res ne '0E0');

    return $res;
}

sub move {
    my ($self, $params) = @_;

    my $result = $self->dbi->dbh->selectall_arrayref(
        'select p.id, coalesce(p.sort_num, p.id)
         from products p
         where cat_id = coalesce(
             (select cat_id from products where id = ?),
             (select cat_id from products where sort_num = ?)
         )
         order by coalesce(p.sort_num, p.id) desc',
        undef,
        $params->{id}, $params->{id}
    );

    #
    # how to  
    #
    my $i = 0;
    my $sort_order_from = $params->{fromPosition};
    my $sort_order_to   = $params->{toPosition};
    my ($from_idx, $to_idx);

    my @sorted = map {
        my $idx = $_;
        my $cur  = $result->[$idx][1];

        if (!$to_idx) {
            $to_idx = $idx if ($cur == $sort_order_to)
                           || ($cur > $sort_order_to && $result->[$idx + 1][1] < $sort_order_to);
        }

        $from_idx = $_ if ($cur == $sort_order_from);
        $cur;
    } 0..$#$result;

    my $to_so_val = splice(@sorted, $to_idx, 1);
    splice(@sorted, $from_idx, 0, $to_so_val);

    my $case = join ' ', ('when id = ? then ?') x scalar @$result;
    my $ph   = join ',', ('?') x scalar @$result;
    my (@bind, @ids);

    map {
        my $cur = $_;
        push @ids, $result->[$cur][0];
        push @bind, $result->[$cur][0], $sorted[$cur];
    } 0..$#$result;

    my $query = "update products
                 set sort_num = case
                 $case
                 end
                 where id in ( $ph )";
    my $res = $self->dbi->dbh->do($query, undef, @bind, @ids); 

    return $res;
}



sub move_to {
    my ($self, $group_id, $ids) = @_;

    my $ph = join ',', ('?') x scalar @$ids;
    my $move_query = "update products set cat_id = ? where id in ($ph)";

    my $res = eval {
        $self->dbi->dbh->do(
            $move_query, undef, $group_id, @$ids
        );
    };

    return $res;
}


1;

