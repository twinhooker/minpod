package Model::images;
use Model -base;
has primary_key => sub { [ 'id' ] };

sub add {
    my ($self, %args) = @_;

    my $img_url   = $args{upload_path} . '/' . $args{img_name};
    my $thumb_url = $args{upload_path} . '/' . $args{thumb_name};

    my $res = $self->insert({
        img_url    => $img_url,
        img_path   => $args{static_path} . $img_url,
        thumb_url  => $thumb_url,
        thumb_path => $args{static_path} . $thumb_url,
    });

    my $id = $self->dbi->dbh->last_insert_id(undef, undef, 'images', 'id');

    return {
        id        => $id,
        img_url   => $img_url,
        thumb_url => $thumb_url
    };
}

1;

