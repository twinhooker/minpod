package MinPod::Users;
use utf8;

use base 'Mojolicious::Controller';

#use Validator::Custom;
use MIME::Lite;
use MIME::Base64;

sub validate_user
{
    my $self = shift;
    
    my $uid = $self->session('uid');
    
    unless ($self->is_user_authenticated)
    {
        $self->session('ref_url' => $self->req->url);
        $self->redirect_to('reg');
        return 0;
    }
    return 1;
}

sub login
{
    my $self = shift;
    
    if ($self->req->method eq 'POST')
    {
        my $params = $self->req->params->to_hash;
        
        unless ($self->authenticate($params->{email}, $params->{password}))
        {
            return $self->render(message => 'Неправильный логин или пароль.', user => $params, params => {});
        }
        my $ref = $self->session('ref_url');
        if (defined $ref)
        {
            delete $self->session->{ref_url};
            return $self->redirect_to($ref);
        }
        return $self->redirect_to('/');
    }
    $self->render(user => {}, params=> {});
}

sub log_out
{
    my $self = shift;
    $self->logout;
    $self->redirect_to('/');
}

sub reg
{
    my $self = shift;
    
 #   my $vc   = Validator::Custom->new;
 #   $vc->register_constraint(
 #       email => sub {
 #           require Email::Valid;
 #           return 0 unless $_[0];
 #           return Email::Valid->address(-address => $_[0], -mxcheck => 1) ? 1 : 0;
 #       }
 #   );
 #   my $vars = { menu => {shi => 'asdf'}, params => {}, user => {}};
    
    if ($self->req->method eq 'POST')
    {
        # $vars->{user} = $self->req->params->to_hash;        
        
        # my $rules = [
        #     email        => {message => 'Введите валидный email адрес.'} => [ 'email', 'not_blank' ],
        #     contact_name => {message => 'Введите ваше имя.' } => [ 'not_blank' ],
        #     phone        => {message => 'Введите телефон для связи.' } => [ 'not_blank' ]
        # ];
        
        # my $vresult = $vc->validate($vars->{user}, $rules);
        # unless ($vresult->is_ok) {
        #     $self->stash('errmsg' => $vresult->messages_to_hash);
        #     return $self->render($vars);
        # }
        
        #$self->recaptcha;
        #if ($self->stash('recaptcha_error')) {
        #    $self->stash('errmsg' => { captcha => 'Тест на робота не пройден. Введите правильно символы'});
        #    return $self->render($vars);
        #}
        
        my $pwd = $self->dbi->model('users')->create_user($vars->{user});
        
        if ($pwd) {
            my $ref_url = $self->session('ref_url');
            $self->authenticate($vars->{user}->{email}, $pwd);
            return $self->redirect_to($ref_url);
        }
        
    }
    
    $self->render($vars);
}

sub unscribe
{
    my $self = shift;
    my $user_hash = $self->param('user_hash');
    
    my $user_mail = decode_base64($user_hash);
    
    my $res = $self->dbi->model('mmail')->unscribe_email($user_mail);
    
    my $message = ($res)  ? 'Вы успешно отписанны от рассылки'
        : 'Возникла ошибка при отписке от рассылки. Пожалуйста сообщите нам об этом по почте для того чтоб отписаться.';
        
    $self->flash(message => $message);
    
    $self->redirect_to('/');
}

1;
