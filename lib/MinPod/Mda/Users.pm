package MinPod::Mda::Users;

use utf8;
use base 'Mojolicious::Controller';

sub list
{
    my $self = shift;
    
    my $list = $self->dbi->model('users')->list;
    
    $self->render(list => $list);
}

sub delete
{
    my $self = shift;
    
    my $uid = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    
    my $res = $self->dbi->model('users')->rem($uid);
    
    my $message = (defined $res) ? 'Клиент успешно удален' : 'Возникла ошибка при удалении';
    
    if ($header && $header eq 'XMLHttpRequest')
    {
        $self->render_json({message => $message, success => $res});
    }
    else
    {
        $self->redirect_to('mda_users_list');
        $self->flash(message => $message);
    }
}
1;