package MinPod::Mda::Cats;
use utf8;
use strict;
use warnings;

use base 'Mojolicious::Controller';


sub mda_list {
    my $self = shift;
    
    my ($list, $total) = $self->dbi->model('categories')->list;
    my $metas = $self->dbi->model('settings')->get_main_metas;

    $self->render(
        list                  => $list,
        main_meta_keywords    => $metas->{main_meta_keywords}->{value},
        main_meta_description => $metas->{main_meta_description}->{value}
    );
}

sub set_image {
    my $self = shift;
    
    my $cat_id = $self->param('cat_id');
    my $p_id   = $self->param('p_id');
    my $header = $self->req->headers->header('X-Requested-With');
    
    my $res = $self->dbi->model('categories')->set_image($cat_id, $p_id);

    $self->render(
        json => { success => $res }
    );
}

sub list_new {
    my $self = shift;

    my ($list, $total) = $self->dbi->model('products')->list_new();

    $self->render(list => $list);
}

sub mda_edit {
    my $self = shift;
    my $params;
    
    my $id = $self->param('id');
    
    my $vars;
    $vars->{cat} = undef;
    $vars->{prods} = undef;

    my ($list)      = $self->dbi->model('categories')->list;
    $vars->{groups} = $list;
    
    if (defined $id) {
        my $limit  = $params->{pplimit} || 10;
        my $offset = ($params->{p} * $limit) || 0;
    
        $vars->{cat} = $self->dbi->model('categories')->get($id);
        $self->render_not_found
            unless $vars->{cat};

        ($vars->{prods}, $vars->{p_total}) = $self->dbi->model('products')->get_cat($id);
    }
    
    $vars->{method} = $self->req->method;
    if ($self->req->method eq 'POST') {
        my $params    = $self->req->params->to_hash;
        my $validator = $self->create_validator;
        $validator->field('name')->required(1);
        $vars->{cat} = $params;
        return $self->render($vars) unless ($self->validate($validator));
        
        my $res = $self->dbi->model('categories')->up($id, $params);
        unless($res)
        {
            $vars->{err} = 'Database error';
            return $self->render($vars);
        }
    }
    
    $self->render(%$vars);
}

sub add {
    my $self = shift;

    my $cat_info = $self->req->params->to_hash;
    my $cat_id   = $self->dbi->model('categories')->add_cat($cat_info);

    $self->render( json =>{ id => $cat_id } );
    
}

sub move_cat {
    my $self = shift;
    my $params = $self->req->params->to_hash;
    $self->dbi->model('categories')->move_cat(@{$params}{qw(parent_id position cat_id)});

    $self->render(json => { restult => 1 });
}

sub update {
    my $self = shift;
    my $id     = $self->param('id');
    my $params = $self->req->params->to_hash;

    my $result = $self->dbi->model('categories')->update(
        $params, where => { id => $id }
    );
    
}

    
sub save {
    my $self = shift;
    my $id     = $self->param('id');
    my $params = $self->req->params->to_hash;

#    if ( defined $id ) {
        $self->dbi->model('categories')->update(
            $params, where => { id => $id }
        );
#    }
#    else {
#        $params->{parent_id} = 0
#            if ( $params->{parent_id} eq '_root' );
#    
#        $self->dbi->model('categories')->insert( $params );
#    }
    
#    $id = $self->dbi->last_insert_id(undef, undef, 'categories', 'id')
#        if ( !$id );

    my $cat = $self->dbi->model('categories')->get( $id );
    #$cat->{parent_id} = '#'
    #    unless $cat->{parent_id};
    $cat->{text} = $cat->{name};
    $cat->{attr} = { href => $self->app->url_for('mda_cats_save') };
    $self->render(json => $cat);
}

sub mda_create {
    my $self = shift;
    
    my $params = $self->req->params->to_hash;
    
    my $validator = $self->create_validator;
    $validator->field('name')->required(1);

    unless ($self->validate($validator))
    {
        return $self->render(template => 'cats/mda_edit', 'cat' => $params);
    }

    my $id = $self->dbi->model('categories')->add($params);
    unless($id)
    {
        return $self->render(template => 'cats/mda_edit', 'cat' => $params, 'err' => 'Database error');
    }
    
    $self->redirect_to('cats_mda_edit', 'id' => $id);
}
sub remove {
    my $self = shift;
    my $id   = $self->param('id');

    my $res = $self->dbi->model('categories')->delete_node($id);

    $self->render(json => {
        result => $res
    });
}


sub delete {
    my $self = shift;
    my ($message);
    
    my $id = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    my $res = $self->dbi->model('categories')->rem($id);
    
    $message = ($res) ? 'Категория удалена успешно' : 'Ошибка при удалении категории';
    
    
    if ($header && $header eq 'XMLHttpRequest')
    {
        $self->render_json({message => $message, success => $res});
    }
    else
    {
        $self->redirect_to('cats_mda_list');
        $self->flash(message => $message);
    }
    
}

sub pos_up
{
    my $self = shift;
    my $id = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    my $res = $self->dbi->model('categories')->weight_up($id);
    
    my $message =  'Ошибка при перемещении категории' unless ($res == 2);
    
    if ($header && $header eq 'XMLHttpRequest')
    {
        $self->render_json({message => $message, success => $res});
    }
    else
    {
        $self->redirect_to('cats_mda_list');
        $self->flash(message => $message);
    }
}

sub pos_down
{
    my $self = shift;
    my $id = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    my $res = $self->dbi->model('categories')->weight_down($id);
    
    my $message =  'Ошибка при перемещении категории' unless ($res == 2);
    
    if ($header && $header eq 'XMLHttpRequest')
    {
        $self->render_json({message => $message, success => $res});
    }
    else
    {
        $self->redirect_to('cats_mda_list');
        $self->flash(message => $message);
    }
}

1;
