package MinPod::Mda::Tags;
use utf8;
use strict;
use warnings;

use base 'Mojolicious::Controller';

sub list {
    my $self = shift;
    my $list = $self->dbi->model('tags')->list;
    $self->render( list => $list );
}

sub edit {
    my $self = shift;
    my $tag_id = $self->param( 'id' );

    my %template_data = ( tag => {}, list => [] );

    if ( defined $tag_id ) {
        my $tag = $self->dbi->model('tags')->get( $tag_id );
        my ( $list, $total ) = $self->dbi->model('tags')->products_for($tag_id);

        $template_data{tag}  = $tag;
        $template_data{list} = $list;
    }

    return $self->render( %template_data );
}

sub add {
    my $self = shift;

    my $tag_data = $self->req->params->to_hash;
    $tag_data->{show_on_main}    = !!$tag_data->{show_on_main};
    $tag_data->{show_on_product} = !!$tag_data->{show_on_product};

    my %upload_field_map = (
        group_image   => 'img_id',
        product_image => 'product_img_id',
    );

    for my $upload ( @{$self->req->uploads} ) {
        if ( $upload->size > 0 ) {
            my $image = $self->upload_tag_image( $upload );
            my $field_name = $upload_field_map{ $upload->name } ;
            $tag_data->{ $field_name } = $image->{id};
        }
    }
    $self->dbi->model('tags')->insert( $tag_data );
    
    $self->redirect_to('mda_tags_list');
}

sub update {
    my $self = shift;

    my $tag_id   = $self->param('id');
    my $tag_data = $self->req->params->to_hash;
    $tag_data->{show_on_main}    = !!$tag_data->{show_on_main};
    $tag_data->{show_on_product} = !!$tag_data->{show_on_product};

    my %upload_field_map = (
        group_image   => 'img_id',
        product_image => 'product_img_id',
    );
    
    for my $upload ( @{$self->req->uploads} ) {
        if ( $upload->size > 0 ) {
            my $image = $self->upload_tag_image( $upload );
            my $field_name = $upload_field_map{ $upload->name } ;
            $tag_data->{ $field_name } = $image->{id};
        }
    }    
    $self->dbi->model('tags')->update( $tag_data, where => { id => $tag_id } );

    $self->redirect_to( 'mda_tagged_products_show', $tag_id );
}

sub upload_tag_image {
    my ( $self, $upload ) = @_;

    my $image_path     = $self->save_image( $upload, { skip_scale => 1 } );
    my $thumbnail_path = $self->create_thumbnail( $image_path );

    my $image = $self->dbi->model('images')->add(
        static_path => $self->image_static_path,
        upload_path => $self->image_upload_path,
        img_name    => $image_path->basename,
        thumb_name  => $thumbnail_path->basename,
    );

    return $image;
}

sub remove {
    my $self = shift;
    my $id   = $self->param('id');

    my $res = $self->dbi->model('tags')->delete(where => { id => $id });
    
    $self->render(json => { success => $res, message => 'Продукт успешно удален'});
    
}

sub remove_tagged {
    my $self = shift;
    my $product_id = $self->param('pid');
    my $tag_id     = $self->param('tid');

    my $res = $self->dbi->dbh->do(
        'delete from tagged_products where tag_id = ? and product_id = ?',
        undef, $tag_id, $product_id
    );
    
    $self->render(json => { success => $res, message => 'Продукт успешно удален'});
}    

sub pos_up {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('tags')->weight_up($id);

    my $message = 'Ошибка при перемещении метки' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });
}

sub pos_down {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('tags')->weight_down($id);

    my $message = 'Ошибка при перемещении метки' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });
}

sub tagged_up {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('tags')->tagged_product_weight_up($id);

    my $message = 'Ошибка при перемещении метки' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });

}

sub tagged_down {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('tags')->tagged_product_weight_down($id);

    my $message = 'Ошибка при перемещении метки' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });

}


1;
