package MinPod::Mda::Mmail;
use utf8;
use common::sense;

use base 'Mojolicious::Controller';

sub list_template
{
    my $self = shift;
    
    my $list = $self->dbi->model('mmail')->list_templates;
    my $unscribe_text = $self->dbi->model('settings')->get_unscribe_text;
       
    $self->render(list => $list, unscribe_text => $unscribe_text);
}

sub show_template
{
    my $self = shift;
    my $id   = $self->param('id');

    my $group_list = $self->dbi->model('mmail')->list_email_groups();
    my $template = $self->dbi->model('mmail')->get_template($id) || {};
    
    $self->render(tmpl => $template, group_list => $group_list);
}

sub write_template
{
    my $self = shift;
    my $id   = $self->param('id');
    
    my $params = $self->req->params->to_hash;
    #say $self->dumper($params);
    my $res    = $self->dbi->model('mmail')->write_template($id, $params);
    
    if ($res)
    {
        return $self->redirect_to('mda_mmail_show_template', id => $res);
    }
    $self->render(tmpl => $params, template => 'mda/mmail/show_template');
}

sub remove_template
{
    my $self = shift;
    my $id   = $self->param('id');
    
    my $res  = $self->dbi->model('mmail')->remove_template($id);
    
    my $message = ($res) ? 'Шаблон удален успешно' : 'Ошибка при удалении шаблона';
    
    $self->render_json({message => $message, success => $res});
}

sub send_template
{
    my $self = shift;
    
    my $tid = $self->param('tid');
    my $group_ids;
    @$group_ids = $self->req->params->param('groups');
    
    say $self->dumper($group_ids);
    unless (scalar (@$group_ids))
    {
        $self->flash(message => 'No group selected');
        return $self->redirect_to($self->req->headers->referrer);
    }
    #use Data::Dumper;
    #$say "ewr" . Dumper(@group_ids);
    my $count = $self->dbi->model('mmail')->add_to_queue($tid, $group_ids);
    
    #say 'ok' if (-e './bin/send_mail.pl');
    my $cmd = $self->app->home->rel_dir('bin/send_mail.pl');
    #say $cmd;
    $self->process(
        $cmd,
        handler  => {
            read => sub {
                my ($stream, $chunk) = @_;
                chomp $chunk;
                $self->app->log->info( sprintf('[%s] %s', $stream->pid, $chunk ));
            },
            close => sub {
                my ($stream) = @_;
                $self->app->log->info( sprintf('[%s] end process', $stream->pid ));
            },
        },
        timeout => 0,
    );
    
    $self->flash(message => $count. ' добавленно в очередь.');
    
    $self->redirect_to($self->req->headers->referrer);
}

sub upload_file
{
    my $self = shift;
    
    #
    # get upload dir
    #
    my $UPLOAD_DIR = $self->app->home->rel_dir('/public/mmail/');
    mkdir $UPLOAD_DIR unless (-d $UPLOAD_DIR);

    #
    # upload file
    #
    my $file = $self->req->upload('attach');
    
    unless ($file)
    {
        return $self->render_json({err => 1, message  => "Upload fail. File is not specified."});
    }
    
    my $file_type = $file->headers->content_type;
    
    my $filename = $file->filename;
    
    my $upload_file = _get_filename($UPLOAD_DIR, $filename);
    $file->move_to($upload_file);
    
    my $file_id = $self->dbi->model('mmail_file')->add_file($upload_file, $file_type, $filename);
    
    $self->render_json({
        name          => $filename,
        fid           => $file_id,
        delete_url    => $self->url_for('mda_mmail_email_remfile', fid => $file_id)
        });
}

sub _get_filename
{
    my ($UPLOAD_DIR, $filename) = @_;
    my $path = $UPLOAD_DIR . '/'  .  $filename;
    #my $path = $UPLOAD_DIR->rel_dir($filename);
    my $i = 0;
    while (-f $path)
    {
        $path = $UPLOAD_DIR . '/'. $filename . "($i)";
        $i++;
    }
    
    return $path;    
}

sub load_types
{
    open my $fh, '<' , 'content-types';
    say "shit" unless ($fh);
    my %types;
    while (<$fh>)
    {
        my ($type,$ext) = split '\s+';
        $types{$type} = $ext;
    }
    return %types; 
}
1;