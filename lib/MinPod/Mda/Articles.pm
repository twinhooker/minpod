package MinPod::Mda::Articles;
use common::sense;
use base 'Mojolicious::Controller';

sub list
{
    my $self = shift;
    
    my $filter;
    
    my $cat = $self->param('id');
    
    $filter->{cat_id} =  $cat if (defined $cat);
    
    my $list = $self->dbi->model('articles')->list($filter);
    
    $self->render(list => $list);
}

sub edit
{
    my $self = shift;

    my $id  = $self->param('id');
    # my $cat = $self->param('cat');

    my $vars;
    $vars->{article} = undef;

    my $cats = {contacts => 3, conditions => 4, about => 5};

    if ( $id =~ m/^\d+$/msx )
    {
        $vars->{article} = $self->dbi->model('articles')->get($id);
        $self->render_not_found unless defined $vars->{article};
    }
    elsif ( $id eq 'contacts' ) {
        $vars->{article} = $self->dbi->model('articles')->get_contacts;
    }
    elsif ( $id eq 'conditions' ) {
        $vars->{article} = $self->dbi->model('articles')->get_conditions;
    }
    elsif ( $id eq 'about') {
        $vars->{article} = $self->dbi->model('articles')->get_about;
    }
    $id = $vars->{article}->{id};

    if ($self->req->method eq 'POST')
    {
        my $params = $self->req->params->to_hash;

        my $validator = $self->create_validator;
        $validator->field('title')->required(1);

        $vars->{article} = $params;
        return $self->render(vars => $vars) unless ($self->validate($validator));

        my $res = $self->dbi->model('articles')->up($params, $id);

        $self->flash('message' => 'Error occured') unless ($res);
    }

    return $self->render(
        template => 'mda/articles/edit',
        vars     => $vars
    );
}

sub addimg
{
    my $self = shift;
    
    my $IMAGE_DIR = "/home/yakudza/devel/min_pod/public/images";
    my $UPLOAD_DIR = $IMAGE_DIR . "/upload";
    
    my $image = $self->req->upload('miu_image_upload_image');
    
    unless ($image) {
        return $self->render_text('FAIL');
    }
    
    my $image_type = $image->headers->content_type;
    my %valid_types = map {$_ => 1} qw(image/gif image/jpeg image/png);
    
    # Content type is wrong
    unless ($valid_types{$image_type}) {
        return $self->render_text('FAIL');
    }
    
    my $exts = {'image/gif' => 'gif', 'image/jpeg' => 'jpg',
                'image/png' => 'png'};
    my $ext = $exts->{$image_type};
    
    my $file_name = create_filename(). ".$ext";
    my $image_file = "$UPLOAD_DIR/" . $file_name;
    while(-f $image_file)
    {
        $file_name = create_filename(). ".$ext";
        $image_file = "$UPLOAD_DIR/" . $file_name;
    }

    $image->move_to($image_file);
    $self->render_text('/images/upload/' . $file_name);
}


sub create_filename
{
    my ($sec, $min, $hour, $mday, $month, $year) = localtime;
    
    $month = $month + 1;
    $year = $year + 1900;
    
    my $rand_num = int(rand 100000);
    
    my $name = sprintf("image-%04s%02s%02s%02s%02s%02s-%05s",
                       $year, $month, $mday, $hour, $min, $sec, $rand_num);
    
    return $name;    
}


sub delete
{
    my $self = shift;
    
    my $id = $self->param('id');
    
    my $res = $self->dbi->model('articles')->rem($id);
    
    $self->flash('message' => 'Error while delete product.')
        unless defined $res;
    
    $self->redirect_to('mda_articles_list');
    
}

1;
