package MinPod::Mda::Products;
use utf8;
use base 'Mojolicious::Controller';
use common::sense;


use GD;

sub index {
    my $self = shift;
}

sub mda_edit {
    my $self = shift;
    my $vars;
    
    my $id     = $self->param('id');
    my $cat_id = $self->param('cat_id');
    
    $vars->{prod} = {};
    
    if (defined($cat_id)) {
        $vars->{prod}->{cat_id} = $cat_id;    
    }
    
    ($vars->{catlist}, $vars->{total_cat})= $self->dbi->model('categories')->list;

    $vars->{tag_list} = $self->dbi->model('tags')->select()->all;
    
    
    if ($self->req->method eq 'POST') {
        $vars->{prod} = $self->req->params->to_hash;
        
        my $validator = $self->create_validator;
        $validator->field('price')->regexp(qr/^\d+\.\d{2}\sруб$/);
        $validator->field('code')->regexp(qr/\d+/);
        
        return $self->render($vars) unless ($self->validate($validator));

        my $tags = delete $vars->{prod}->{tags};
        $self->dbi->model('tags')->tags_update_for($id, $tags);
        
        my $res = $self->dbi->model('products')->up($vars->{prod}, $id);
        unless ($res == 1) {
            $vars->{err} = 'Database error';            
            return $self->render(%$vars);
        }        
    }

    if (defined($id)) {
        $vars->{prod} = $self->dbi->model('products')->get($id);
        $self->render_not_found unless $vars->{prod};
    }
    
    $self->render(%$vars);
}

sub add {
    my $self             = shift;
    my $product          = $self->req->params->to_hash;
    $product->{in_stock} = ($product->{in_stock}) ? 1 : 0;

    my $product = $self->dbi->model('products')->add($product);
    $self->render( json => $product );    
}

sub save {
    my $self    = shift;
    my $id      = $self->param('id');
    my $product = $self->req->params->to_hash;
    my $tags    = delete $product->{tags};

    $product->{in_stock}  = ( $product->{in_stock}  ) ? 1 : 0;
    $product->{new_price} = ( $product->{new_price} ) ? $product->{new_price} : undef;

    $self->dbi->model('products')->update($product, where => {id => $id});
    $self->dbi->model('tags')->update_tags_for($id, $tags);

    $product = $self->dbi->model('products')->get($id);

    $self->render(json => $product);
}

sub mda_delete {
    my $self = shift;
    
    my $id      = $self->param('id');
    my $res     = $self->dbi->model('products')->delete(where => { id => $id });
    my $message = (defined $res) ? 'Запись удалена успешно' : 'Error while delete product.';
    
    $self->render(json => {message => $message, success => $res});
}

sub mda_add_img
{
    my $self = shift;

    my $HOME_DIR = $self->app->home;
    
    my $IMAGE_DIR = $HOME_DIR . "/public/images";
    my $UPLOAD_DIR = $IMAGE_DIR . "/upload";
    
    my $image = $self->req->upload('image');
    
    unless ($image) {
        return $self->render(json => {
            err      => 1,
            message  => "Upload fail. File is not specified."
        });
    }
    
    my $image_type = $image->headers->content_type;
    my %valid_types = map {$_ => 1} qw(image/gif image/jpeg image/png);
    
    # Content type is wrong
    unless ($valid_types{$image_type}) {
        return $self->render(json => {
            err      => 1,
            message  => "Upload fail. Content type is wrong."
        });
    }
    
    my $exts = {'image/gif' => 'gif', 'image/jpeg' => 'jpg',
                'image/png' => 'png'};
    my $ext = $exts->{$image_type};
    
    my $file_name = create_filename(). ".$ext";
    my $image_file = "$UPLOAD_DIR/" . $file_name;
    while(-f $image_file)
    {
        $file_name = create_filename(). ".$ext";
        $image_file = "$UPLOAD_DIR/" . $file_name;
    }

    $image->move_to($image_file);
    
    my $thumb_name = create_thumb($file_name, $UPLOAD_DIR, $IMAGE_DIR . '/product_border.png');
    
    $self->render(json => {
        name          => $image_file,
        url           => '/images/upload/' . $file_name,
        thumbnail_url => '/images/upload/thumb_' . $thumb_name,
        delete_url    => $self->url_for('prods_mda_delimg', file => $file_name)
    });
}

sub create_thumb {
    my ($im_filename, $path, $wm_path) = @_;
    
    my $thumb_filename = $im_filename;
    $thumb_filename =~ s/\..*$/\.png/;
    my $outfile = $path . '/thumb_' . $thumb_filename;
    
    GD::Image->trueColor(1);
    
    my $im = GD::Image->new( $path . '/'. $im_filename) or 
        die(sprintf("Failed to open \"%s\"", $path . '/'. $im_filename));        
    my $wm = GD::Image->new( $wm_path );
    $wm->saveAlpha(1);
    my $im_h = int (218 * $im->height / $im->width);
    my $wm_h = int (231 * $im->height / $im->width);
    
    my $image = GD::Image->new(231, $wm_h);
    my $clear = $im->colorAllocateAlpha(255, 255, 255, 127);
    
    $image->saveAlpha(1);

    $image->alphaBlending(0);
    
    $image->fill(50,50,$clear);
    
    $image->alphaBlending(1);
    $image->saveAlpha(1);

    $image->copyResized($im, 5, 1, 0, 0, 220, $im_h, $im->width, $im->height);
    $image->copyResized($wm, 0, 0, 0, 0, 231, $wm_h, $wm->width, $wm->height);

    open my $out, ">$outfile";
    print $out $image->png;
    close $out;
    
    return $thumb_filename;
}

sub create_filename
{
    my ($sec, $min, $hour, $mday, $month, $year) = localtime;
    
    $month = $month + 1;
    $year = $year + 1900;
    
    my $rand_num = int(rand 100000);
    
    my $name = sprintf("image-%04s%02s%02s%02s%02s%02s-%05s",
                       $year, $month, $mday, $hour, $min, $sec, $rand_num);
    
    return $name;    
}

sub new_pos_up {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('products')->weigt_up($id, 1);

    my $message = 'Ошибка при перемещении продукта' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });
}

sub new_pos_down {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('products')->weight_down($id, 1);

    my $message = 'Ошибка при перемещении продукта' unless ($res == 2);

    $self->render(json => {
        message => $message,
        success => $res
    });
}

sub set_new_img {
    my $self = shift;

    my $id        = $self->param('id');
    my $product   = $self->dbi->model('products')->get($id);
    my $small_img = ''; 
    
    if ( $product ) {
        $small_img = $product->{img_small};
        $self->dbi->model('settings')->save_settings({
            'new_img' => $small_img
        });
    }
    
    $self->render(json => {
        new_img => $small_img
    });

    return;
}

sub new_remove {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('products')->remove_from_new($id);
    
    $self->render(json => { res => $res });
    
    return;
}

sub move {
    my $self = shift;
    my $data = $self->req->params->to_hash;

    my $res = $self->dbi->model('products')->move($data);

    return $self->render( json => {
        res => $res ? 1 : 0
    });
    
}



sub pos_up {
    my $self = shift;
    
    my $id     = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    my $res    = $self->dbi->model('products')->weight_up($id);
    
    my $message =  'Ошибка при перемещении продукта' unless ($res == 2);
    
    if ($header && $header eq 'XMLHttpRequest') {
        $self->render_json({message => $message, success => $res});
    }
    else {
        $self->redirect_to('cats_mda_list');
        $self->flash(message => $message);
    }
}

sub pos_down {
    my $self = shift;
    
    my $id     = $self->param('id');
    my $header = $self->req->headers->header('X-Requested-With');
    my $res    = $self->dbi->model('products')->weight_down($id);
    
    my $message =  'Ошибка при перемещении продукта' unless ($res == 2);
    
    if ($header && $header eq 'XMLHttpRequest') {
        $self->render(json => {message => $message, success => $res});
    }
    else {
        $self->redirect_to('cats_mda_list');
        $self->flash(message => $message);
    }
}

sub toggle {
    my $self = shift;
    
    my $id  = $self->param('id');
    my $res = $self->dbi->model('products')->toggle($id);
    
    $self->render(json => { success => $res });
}

sub toggle_new {
    my $self = shift;

    my $id  = $self->param('id');
    my $res = $self->dbi->model('products')->toggle_new($id);

    $self->render(json => { success => $res });
}

sub new_set_image {
    my $self = shift;

    my $product_id = $self->param('id');
    my $product    = $self->dbi->model('products')->get($product_id);
    
    my $res        = $self->dbi->model('settings')->save_settings({'newcat_img' => $product->{img_small}});

    $self->render(json => { success => $res });
}

sub actions {
    my $self = shift;

    my $referrer = $self->req->headers->referrer;
    my $params   = $self->req->params->to_hash;
    my $group_id = $self->req->param('group_id');

    my @ids      = grep { /\d+/ } keys %$params;

    my $result = $self->dbi->model('products')->move_to($group_id, \@ids);
    
    $self->redirect_to($referrer);
}



1;
