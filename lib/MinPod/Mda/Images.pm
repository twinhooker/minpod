package MinPod::Mda::Images;
use strict;
use warnings;
use Try::Tiny;
use Readonly;
use FindBin;
use Image::Imlib2;

Readonly my $STATIC_PATH  => "$FindBin::Bin/../public";
Readonly my $UPLOAD_PATH  => "/images/upload";

use base 'Mojolicious::Controller';

sub upload {
    my $self = shift;

    my $image_upload = $self->req->upload('image');
    return $self->render(json => { error => "Upload fail. File is not specified." })
        unless ( $image_upload );

    try {
        my $file_name  = $self->save_image( $image_upload );
        my $thumb_name = $self->create_thumbnail( $file_name );
        my $image = $self->dbi->model('images')->add(
            static_path => $STATIC_PATH,
            upload_path => $UPLOAD_PATH,
            img_name    => $file_name->basename,
            thumb_name  => $thumb_name->basename,
        );

        $self->render( json => $image );
    }
    catch {
        $self->render( json => { error => $_ } );
    };

    return;
}

# sub store_image {
#     my ($self, $image) = @_;

#     my $image_type  = $image->headers->content_type;
#     my %valid_types = map { $_ => 1 } qw( image/gif image/jpeg image/png );
#     die "Upload fail. Content type is wrong."
#         if !$valid_types{$image_type};

#     my $exts = {
#         'image/gif'  => 'gif',
#         'image/jpeg' => 'jpg',
#         'image/png'  => 'png'
#     };
#     my $ext = $exts->{$image_type};

#     my $original_name = generate_filename($ext);
#     my $original_path = $STATIC_PATH . $UPLOAD_PATH . "/original_$original_name"; 
#     $image->move_to($original_path);

#     my $image_name = generate_filename('png');
#     my $image_obj  = GD::Image->new($original_path) || die;
#     my ($new_width, $new_height) = calculate_size($image_obj->width, $image_obj->height);

#     my $new_image = GD::Image->new($new_width, $new_height);
#     $new_image->copyResized($image_obj, 0, 0, 0, 0, $new_width, $new_height, $image_obj->getBounds);

#     open my $out, '>', $STATIC_PATH . $UPLOAD_PATH . "/$image_name";
#     print $out $new_image->png;
#     close $out;

#     return $image_name;
# }

# sub calculate_size {    
#     my ($width, $height) = @_;
#     my ($new_width, $new_height) = (800, 600);
#     my $RATIO = 800 / 600;
    
#     if ($width / $height < $RATIO) {
#         $new_width  = 600 * $width / $height; 
#     }
#     else {
#         $new_height = 800 * $height / $width;
#     }

#     return ($new_width, $new_height);
# }

# sub create_thumb {
#     my ( $self, $file_name ) = @_;

#     my $im = GD::Image->new($STATIC_PATH . $UPLOAD_PATH . "/$file_name") or 
#         die "Can't open file";        

#     my $new_height = int ( 231 * $im->height / $im->width );
#     my $image = GD::Image->new( 231, $new_height );
#     $image->copyResized($im, 0, 0, 0, 0, 231, $new_height, $im->getBounds);

#     my $thumb_name = generate_filename('png');

#     open my $out, '>', $STATIC_PATH . $UPLOAD_PATH . "/$thumb_name";
#     print $out $image->png;
#     close $out;

#     return $thumb_name;
# }

# sub generate_filename {
#     my $ext = shift;
#     my $rand_num = int(rand 100000);
    
#     my ($sec, $min, $hour, $mday, $month, $year) = localtime;
#     $month = $month + 1;
#     $year  = $year + 1900;

#     my $name = sprintf(
#         "image-%04s%02s%02s%02s%02s%02s-%05s.%s",
#         $year, $month, $mday, $hour, $min, $sec, $rand_num, $ext
#     );

#     my $image_path = $STATIC_PATH . $UPLOAD_PATH . "/$name";
    
#     return $name unless ( -f $image_path );    
#     return generate_filename( $ext );    
# }

sub remove {
}

sub cleanup {
}

1;
