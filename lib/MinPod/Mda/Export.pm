package MinPod::Mda::Export;
use Text::CSV;
use Mojo::Base 'Mojolicious::Controller';

sub form {
    my $self = shift;

    my $categories = $self->dbi->model('categories')->get_root;
    my $tags       = $self->dbi->model('tags')->list;

    $self->render(
        categories => $categories,
        tags       => $tags,
    );
}

sub do {
    my $self   = shift;
    my $params = $self->req->params->to_hash;

    my $requested_cats = delete $params->{categories};
    my $requested_tags = delete $params->{tags};
    my $export_type    = ( delete $params->{xls} ) ? 'xlsx'
                       : ( delete $params->{csv} ) ? 'csv'
                       : ( delete $params->{xml} ) ? 'xml'
                       :                             undef;
    
    $requested_cats = [ $requested_cats ]
        if ( defined $requested_cats
        and  ref $requested_cats ne 'ARRAY' );
    $requested_tags = [ $requested_tags ]
        if ( defined $requested_tags
        and  ref $requested_tags ne 'ARRAY' );

    my @categories = map {
        my $cat_id = $_;
        
        my $cats = $self->dbi->model('categories')->get_childs( $cat_id );
        my @cats = map { $_->{id} } @$cats;
        push @cats, $cat_id;
        @cats;
    } @$requested_cats;

    my $products = $self->dbi->model('products')->export(
        ( cats => \@categories )    x!! @categories,
        ( tags => $requested_tags ) x!! ( $requested_tags && @$requested_tags ),
        fields => $params,
        type   => $export_type
    );

    my %content_types = (
        csv  => 'text/csv',
        xlsx => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    
    $self->res->headers->content_type( $content_types{ $export_type } . 'name=minpod.' . $export_type );
    $self->res->headers->content_disposition('attachment;filename=minpod.' . $export_type );

    $self->render( data => $products );
}

1;
