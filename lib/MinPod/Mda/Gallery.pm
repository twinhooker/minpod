package MinPod::Mda::Gallery;
use strict;
use warnings;
use utf8;
use base 'Mojolicious::Controller';

sub main {
    my $self = shift;

    my $list = $self->dbi->model('tags')->select( append => 'order by coalesce(sort_num, id)' )->all;

    $self->render(tags => $list);
}

sub show_product {
    my $self = shift;
    my $product_id = $self->param('id');

    $self->render(
        json => $self->dbi->model('products')->get( $product_id )
    );
}

sub product_list {
    my $self   = shift;
    my $cat_id = $self->param('cat_id') || 1;

    my ($list, $total)
        = $self->dbi->model('products')->get_cat($cat_id);

    my @list = map {
        # my $row = [ @{$_}{qw(sort_order id code name price)} ];
        my %row;
        @row{qw(id sort_order code name price in_stock)}
            = @{$_}{qw(id sort_order code name price in_stock)}; 
        my $id         = $_->{id};
        my $in_stock   = ($_->{in_stock}) ? 'Да' : 'Нет';
        $row{id}      .= "<input type='hidden' class='row_id' name='id' value='$id'>";
        $row{in_stock} = $in_stock;
        
        \%row;
    } @$list;
    
    $self->render( json => { aaData => \@list });
}

sub nested_jstree {
    my $self      = shift;
    my $cat_id    = $self->param('cat_id');
    my $node_list = $self->dbi->model('categories')->tree_tree($cat_id);
    
    $self->render(json => $node_list);
}

sub tree_as_list {
    my $self = shift;
    my $cat_tree = $self->dbi->model('categories')->get_tree_as_list;
    $self->render(json => $cat_tree);
}

1;


