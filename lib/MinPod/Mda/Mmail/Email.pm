package MinPod::Mda::Mmail::Email;
use utf8;
use common::sense;
use Encode;


use base 'Mojolicious::Controller';

use Email::Valid;

sub list
{
    my $self = shift;
    
    my $group_id = $self->param('gid');
    my $list = [];
    my $total;
    my $page  = $self->req->param('p');
    #my $query = $self->req->param('q');
    if ($group_id)
    {
        ($list, $total)  = $self->dbi->model('mmail')->list_emails($group_id, $page);
    }
    my $group_list = $self->dbi->model('mmail')->list_email_groups();
    
    $self->render(list => $list, total => $total, group_list => $group_list);
}


sub import
{
    my $self = shift;
    
    my $gid = $self->req->param('gid');
    say $gid;
    my $import_file = $self->req->upload('import_file');
    
    unless ($import_file)
    {
        $self->flash(message  => "Upload fail. File is not specified.");
        return $self->redirect_to('mda_mmail_email_grp_list', gid => $gid);
    }
    
    my $file_type = $import_file->headers->content_type;
    
    unless ($file_type eq 'text/plain')
    {
        $self->flash(message  => "Upload fail. wrong file-type.");
        return $self->redirect_to('mda_mmail_email_grp_list', gid => $gid);
    }
    
    my $data = $import_file->slurp;

    for my $email (split /^/, $data)
    {
        chomp $email;
        
        if (Email::Valid->address($email))
        {
            $self->dbi->model('mmail')->add_email($gid, $email);
        }   
        
    }
    $self->flash(message => "Emails uploaded");
    return $self->redirect_to('mda_mmail_email_grp_list', gid => $gid);
}

sub add_group
{
    my $self = shift;
    
    my $grp_name = $self->req->param('grp_name');
    
    my $res = $self->dbi->model('mmail')->add_group($grp_name);
    
    if ($res)
    {
        my $group = {id => $res, name => $grp_name};
        return $self->render(
            template => 'mda/mmail/email/group_item',
            group => $group
        );
    }
    
    $self->render_text('');
}

sub switch
{
    my $self = shift;
    
    my $id = $self->param('id');
    
    my $res = $self->dbi->model('mmail')->switch($id);
    
    $self->render_json({success => $res});
}

sub remove_email
{
    my $self = shift;
    my $id   = $self->param('id');
    
    my $res  = $self->dbi->model('mmail')->remove_email($id);
    
    my $message = ($res) ? 'Адрес удален успешно' : 'Ошибка при удалении адреса';
    
    $self->render_json({message => $message, success => $res});
}

sub remove_group
{
    my $self = shift;
    my $id   = $self->param('id');
    
    my $res  = $self->dbi->model('mmail')->remove_group($id);
    
    my $message = ($res) ? 'Группа удалена успешно' : 'Ошибка при удалении группы';
    
    $self->render_json({message => $message, success => $res});
}

1;