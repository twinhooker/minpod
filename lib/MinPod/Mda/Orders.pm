package MinPod::Mda::Orders;
use utf8;
use base 'Mojolicious::Controller';

sub list {
    my $self = shift;
    
    my $list = $self->dbi->model('orders')->list;

    $self->render(list => $list);
}

sub show {
    my $self = shift;

    my $id    = $self->param('id');
    my $order = $self->dbi->model('orders')->get_order( $id );
    my $user  = $self->dbi->model('users')->get_order_user( $id );

    $self->render( order => $order, user => $user );
}

sub delete
{
    my $self = shift;
    
    my $id = $self->param('id');
    
    my $res = $self->dbi->model('orders')->remove($id);
    
    if ($res)
    {
        return $self->render_json({success => 1, message => 'Заказ успешно удален.'});
    }
    else
    {
        return $self->render_json({success => 0, message => 'Произошла ошибка во время удаления.'});
    }
}

sub complete
{
    my $self = shift;
    
    my $id = $self->param('id');
    
    
    my $res = $self->dbi->model('orders')->complete($id);
    
    if ($res)
    {
        return $self->render_json({success => 1, message => 'Заказ завершен.'});
    }
    else
    {
        return $self->render_json({success => 0, message => 'Произошла ошибка'});
    }
}

1;
