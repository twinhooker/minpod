package MinPod::Mda;
use utf8;
use base 'Mojolicious::Controller';

sub main
{
    my $self = shift;
    
    $self->render();
}

sub auth
{
    my $self = shift;
    
    unless ($self->session('is_adm') == 1)
    {
        $self->session('ref_url' => $self->req->url);
        $self->redirect_to('mda_login');
        return 0;
    }
    
    my ($cats, $total) = $self->dbi->model('categories')->list;
    
    $self->stash(cats => $cats);
    
    return 1;
}

sub login
{
    my $self = shift;
    
    if ($self->req->method eq 'POST')
    {
        my $params = $self->req->params->to_hash;
        
        unless ($params->{username} &&
                $params->{username} eq 'admin' &&
                $params->{password} &&
                $params->{password} eq 'jungle321')
        {
            return $self->render(message => 'Неправильный логин или пароль.', user => $params);
        }
        my $ref = $self->session('ref_url');
        $self->session('is_adm' => 1);
        if (defined $ref)
        {
            delete $self->session->{ref_url};
            return $self->redirect_to($ref);
        }
        return $self->redirect_to('/');
    }
    $self->render(user => {});
}

sub logout
{
    my $self = shift;
    delete $self->session->{is_adm};
    $self->redirect_to('/');
}

sub save_settings
{
    my $self = shift;
    
    my $params = $self->req->params->to_hash;
    my $res = $self->dbi->model('settings')->save_settings($params);
    
    my $message = ($res) ? 'Настройки сохранены успешно' : 'Произошла ошибка во время сохранения';
    
    $self->flash(message => $message);
    
    $self->redirect_to($self->req->headers->referrer);
}

1;