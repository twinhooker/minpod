package MinPod::Gallery;

use base 'Mojolicious::Controller';
use Data::Dumper;
use common::sense;

sub tag {
    my $self = shift;
    my $page   = $self->req->param('p') || 0;
    my $tag_id = $self->param('id');
    my $offset = $page * 10;

    my $tag = $self->dbi->model('tags')->select(where => { id => $tag_id })->one;
    my ($product_list, $total) =
        $self->dbi->model('tags')->products_for($tag_id , 10, $offset);

    $self->title($tag->{name});

    $self->render(
        params      => {
            keywords    => $tag->{meta_tags},
            description => $tag->{description},
            menu        => { main => 1 },
            list        => $product_list,
            total       => $total,
        },
        template    => 'gallery/cat',
    );
}

sub cat {
    my $self = shift;
    
    my $page   = $self->req->param('p') || 0;
    my $id     = $self->param('id');
    my $params = { menu => {main => 1} };
    my $offset = $page * 10;
    
    my  $cat           = $self->dbi->model('categories')->get($id);
    my ($list, $total) = $self->dbi->model('products')->get_cat($id, 10, $offset, 'desc');   
    my  $breadcrumbs   = $self->dbi->model('categories')->get_path($id);
    my  $child_cats    = $self->dbi->model('categories')->get_childs($id);
    
    $self->title($cat->{name});
    $self->stash('keywords' => $cat->{meta_tags});
    $self->stash('description' => $cat->{description});
    
    $params->{list}  = $list;
    $params->{total} = $total;
    
    $self->render(
        params      => $params,
        breadcrumbs => $breadcrumbs,
        child_cats  => $child_cats
    );
}

sub list_new {
    my $self = shift;

    my $page = $self->req->param('p') || 0;

    my $id = $self->param('id');
    my $params = { menu => {main => 1} };

    my $offset = $page * 10;

    my ($list, $total) = $self->dbi->model('products')->list_new(10, $offset);

    $self->title("Новые поступления");
    $params->{list}  = $list;
    $params->{total} = $total;
    
    $self->render(template => 'gallery/cat', params => $params);
}

sub show
{
    my $self   = shift;
    my $id     = $self->param('id');
    my $cat_id = $self->param('cat_id');
    
    my $page = $self->dbi->model('products')->get_pos($id, $cat_id);
    
    my $url = Mojo::URL->new;
    $url->path('/gallery/' . $cat_id);
    $url->query(p => $page);

    #print $url . "\n";    
    $self->redirect_to($url . '#' . $id);
    ####
    # first create description 
    #my $item = $self->dbi->model('products')->get($id);
    
    #$self->render(item => $item);
}



1;
