package MinPod::MinPod;
use common::sense;

use Mojo::URL;
use Mojo::UserAgent;
use base 'Mojolicious::Controller';

sub main {
    my $self = shift;

    my $tags       = $self->dbi->model('tags')->gallery_list;
    my $categories = $self->dbi->model('categories')->get_root;
    my $tree       = $self->dbi->model('categories')->tree_tree( 0 );
    my $popular    = $self->dbi->model('tags')->products_for( 6 );
    my $metas      = $self->dbi->model('settings')->get_main_metas;
    my $newcat_img = $self->dbi->model('settings')->get_value_for( 'newcat_img' );
    my $root_list  = $self->dbi->model('categories')->get_root;

    $self->render(
        tag_list    => $tags,
        cat_list    => $categories,
        cat_tree    => $categories,
        keywords    => $metas->{main_meta_keywords}{value},
        description => $metas->{main_meta_description}{value},
        popular     => $popular,
        sidebar_list => $root_list,
        sidebar_tags => $tags,
    );
}

# sub show_cat {
#     my $self   = shift;
#     my $cat_id = $self->param('id');

#     my $cat        = $self->dbi->model('categories')->get($cat_id);
#     my ($products) = $self->dbi->model('products'  )->get_cat($cat_id, undef, undef, 'asc');
#     my $path       = $self->dbi->model('categories')->get_path($cat_id);
#     my $child_cats = $self->dbi->model('categories')->get_childs($cat_id);
#     my $root_list  = $self->dbi->model('categories')->get_root;

#     my @products = sort { int $b->{in_stock} } @$products;

#     $self->title($cat->{name});

#     $self->render(
#         category     => $cat,
#         breadcrumbs  => $path,
#         child_cats   => $child_cats,
#         products     => \@products,
#         sidebar_list => $root_list,
#     );
# }

sub show_tag {
    my $self   = shift;
    my $tag_id = $self->param('id');

    my $tag          = $self->dbi->model('tags')->select(where => { id => $tag_id })->one;
    my $product_list = $self->dbi->model('tags')->products_for($tag_id);
    my $tag_list     = $self->dbi->model('tags')->gallery_list;
    my $root_list    = $self->dbi->model('categories')->get_root;

    $self->title($tag->{name});
    $tag->{url} = $self->url_for('show_tag', $tag_id);

    $self->render(
        tag          => $tag,
        breadcrumbs  => [ $tag ],
        products     => $product_list,
        sidebar_list => $root_list,
        tag_list     => $tag_list
    );
}

sub news
{
    my $self = shift;

    my $params = { menu => {news => 1} };

    $params->{list} = $self->dbi->model('articles')->list({ cat_id => 1 });
    $self->title('Новости');

    $self->render(params => $params)
}

sub useful
{
    my $self = shift;
    
    my $params = { menu => {useful => 1} };
    
    $params->{list} = $self->dbi->model('articles')->list({ cat_id => 2 });
    $self->title('Полезное');
    
    $self->render(params => $params);
}

sub delivery {
    my $self = shift;

    my $tag_list  = $self->dbi->model('tags')->gallery_list;
    my $root_list = $self->dbi->model('categories')->get_root;

    $self->title('Доставка');

    $self->render(
        sidebar_list => $root_list,
        tag_list     => $tag_list,
    );
}
sub contacts {
    my $self = shift;

    my $params = {menu => {contacts => 1} };
    my $tag_list  = $self->dbi->model('tags')->gallery_list;
    my $root_list = $self->dbi->model('categories')->get_root;

    $params->{article} = $self->dbi->model('articles')->get_contacts;
    $self->title('Контакты');

    $self->render(
        params => $params,
        sidebar_list => $root_list,
        tag_list     => $tag_list,
    );
}

sub conditions {
    my $self = shift;
    my $params = { menu => {conditions => 1} };

    $params->{article} = $self->dbi->model('articles')->get_conditions;
    my $tag_list  = $self->dbi->model('tags')->gallery_list;
    my $root_list = $self->dbi->model('categories')->get_root;

    $self->title('Условия работы');

    $self->render(
        params       => $params,
        sidebar_list => $root_list,
        tag_list     => $tag_list,
    );
}

sub about {
    my $self = shift;
    my $params = { menu => { about => 1} };

    $params->{article} = $self->dbi->model('articles')->get_about;
    my $tag_list  = $self->dbi->model('tags')->gallery_list;
    my $root_list = $self->dbi->model('categories')->get_root;

    $self->render(
        params       => $params,
        sidebar_list => $root_list,
        tag_list     => $tag_list,
    );
}

sub article
{
    my $self = shift;
    
    my $id = $self->param('id');
    
    my $params = {};
    $params->{article} = $self->dbi->model('articles')->get_article($id);
    
    if ($params->{article}->{cat_id} == 2)
    {
        $params->{menu} = {useful  => 1};
    }
    else
    {
        $params->{menu} = {news => 1};
    }
    #my $params = { menu => {news => 1} };
    
    $self->render(params => $params)
}

sub search {
    my $self = shift;

    my $page       = $self->req->param('p');
    my $search_str = $self->req->param('search_str');

    unless ( $search_str ne '' || $search_str ) {
        $self->redirect_to( $self->req->headers->header('reffer') );
    }
    my $cat_res = $self->dbi->model('categories')->search_phrase($search_str);
    my @cat_res = sort {
        $a->{search_weight} <=> $b->{search_weight}
    } values %{ $cat_res };

    my $products = $self->dbi->model('products')->search_phrase($search_str);
    my @products = sort {
        $a->{search_weight} <=> $b->{search_weight}
    } values %{ $products };

    my %cat_product_map;
    my @cat_order;
    for my $product (@products) {
        my $cat_id = $product->{cat_id};
        if (!$cat_product_map{$cat_id}) {
            push @cat_order, $cat_id;
            $cat_product_map{$cat_id} = {
                path     => $self->dbi->model('categories')->get_path($cat_id),
                products => [ $product ]
            };
        }
        else {
            push @{$cat_product_map{$cat_id}{products}}, $product;
        }
    }
    my @cats_products = @cat_product_map{@cat_order};

    my $tag_list     = $self->dbi->model('tags')->gallery_list;
    my $sidebar_list = $self->dbi->model('categories')->get_root;

    $self->render(
        params        => { menu => {} },
        product_res   => \@products,
        cats_products => \@cats_products,
        cat_res       => \@cat_res,
        tag_list      => $tag_list,
        sidebar_list  => $sidebar_list,
    );
}

sub suggest {
    my $self = shift;

    my $string = $self->req->param('search_str');
    my $res    = $self->dbi->model('products')->suggestion( $string );
    $res       = [] unless scalar @$res;

    return $self->render(json => $res);
}

sub weather_proxy {
    my $self = shift;

    my $location = $self->req->param('location');
    my $metric   = $self->req->param('metric');
    
    my $url = Mojo::URL->new('http://wwwa.accuweather.com/adcbin/forecastfox/weather_data.asp');
    $url->query({
        location => $location,
        metric   => $metric,
    });

    my $ua = Mojo::UserAgent->new();

    say $self->dumper($ua->get($url));
    
}

1;
