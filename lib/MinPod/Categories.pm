package MinPod::Categories;

use common::sense;
use base 'Mojolicious::Controller';

sub list {
    my $self = shift;

    my $root_list = $self->dbi->model('categories')->get_root;
    my $tags      = $self->dbi->model('tags')->gallery_list;

    $self->render(
        sidebar_list => $root_list,
        categories   => $root_list,
        category     => { id => 0 },
        tag_list     => $tags,
    );
}

sub show {
    my $self = shift;

    my $cat_id = $self->param('id');

    my $path         = $self->dbi->model('categories')->get_path( $cat_id );
    my $cat          = $self->dbi->model('categories')->get( $cat_id );
    my $tags         = $self->dbi->model('tags')->gallery_list;
    my ( $products ) = $self->dbi->model('products')->get_cat( $cat_id, undef, undef, 'desc' );
    my $path         = $self->dbi->model('categories')->get_path( $cat_id );
    my $child_cats   = $self->dbi->model('categories')->get_childs( $cat_id );
    my $root_list    = $self->dbi->model('categories')->get_root;

    my @products = sort { int $b->{in_stock} } @$products;

    $self->render(
        category     => $cat,
        breadcrumbs  => $path,
        child_cats   => $child_cats,
        products     => \@products,
        sidebar_list => $root_list,
        tag_list     => $tags,
        breadcrumbs  => $path,
    );
}

1;
