package MinPod::Products;

use common::sense;
use base 'Mojolicious::Controller';

sub show {
    my $self = shift;

    my $product_id = $self->param('id');
    my $product    = $self->dbi->model('products')->get( $product_id );
    my $path       = $self->dbi->model('categories')->get_path( $product->{cat_id} );
    my $tags       = $self->dbi->model('tags')->gallery_list;
    my $root_list  = $self->dbi->model('categories')->get_root;

    $self->render(
        product      => $product,
        breadcrumbs  => $path,
        tag_list     => $tags,
        sidebar_list => $root_list,
    );
}

1;
