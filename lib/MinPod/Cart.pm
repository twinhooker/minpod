package MinPod::Cart;
use strict;
use utf8;
use base 'Mojolicious::Controller';
use MIME::Lite;
use feature "switch";
use Template::Tiny;
use List::Util qw( sum );

sub show_cart {
    my $self = shift;

    my $ordered_products_map = $self->session('cart') || {};
    my @product_ids          = keys %$ordered_products_map;
    my $product_list         = $self->dbi->model('products')->cart_list( @product_ids );
    my $root_list            = $self->dbi->model('categories')->get_root;
    my $tags                 = $self->dbi->model('tags')->gallery_list;

    my $total;
    my @cart = map {
        my $product         = $_;
        my $product_price   = $product->{new_price} || $product->{price};
        my $quantity        = $ordered_products_map->{ $product->{id} };
        my $sum             = $quantity * $product_price;
        $total             += $sum;
        $product->{sum}     = $sum;
        $product->{ordered} = $quantity;
        $product->{price}   = $product_price;
        $product;
    } @$product_list;

    $self->render(
        sidebar_list => $root_list,
        tag_list     => $tags,
        cart         => \@cart,
        total        => $total
    );
}

sub cart {
    my $self = shift;

    my $header  = $self->req->headers->header('X-Requested-With');
    my $id = $self->req->param('id');

    $self->session(cart => {})
        unless defined $self->session('cart');
    $self->session(cart_price => 0)
        unless defined $self->session('cart_price');

    # save sessions for people adding to cart for one week
    $self->session( expires => time + 604800 );

    my $product = $self->dbi->model('products')->get($id);

    if (defined $product) {
        $self->stash('product' => $product);
        return 1;
    }
    return 0;
}

sub add {
    my $self = shift;

    my ($count) = $self->req->param('count') =~ /(\d+)/;
    my $product = $self->stash('product');

    $self->session->{cart}->{$product->{id}} += $count;
    $self->session->{cart_price} += $product->{price} * $count;

    ####
    # long js response
    #
    my $header  = $self->req->headers->header('X-Requested-With');
    if ($header && $header eq 'XMLHttpRequest') {
        my $cart_total = 0;
        $cart_total   += $_ for values %{$self->session('cart')};
        return $self->render(json => {
            count      => $self->session('cart')->{$product->{id}},
            cart       => $cart_total,
            cart_price => $self->session('cart_price'),
            cart_key_cnt => scalar keys %{$self->session('cart')}
        });
    }
    my $referrer = $self->req->headers->referrer;
    $self->redirect_to($referrer);
}

sub modify {
    my $self = shift;
    my ($count) = $self->req->param('count') =~ /(\d+)/;
    my $product = $self->stash('product');

    my $count_before = $self->session('cart')->{$product->{id}};
    $self->session('cart')->{$product->{id}} = $count;

    ####
    # long js response
    #
    my $header  = $self->req->headers->header('X-Requested-With');
    if ($header && $header eq 'XMLHttpRequest') {
        my $cart_total = 0;
        $cart_total   += $_ for values %{$self->session('cart')};
        return $self->render(json => {
            count      => $self->session('cart')->{$product->{id}},
            cart       => $cart_total,
            cart_price => $self->session('cart_price'),
            cart_key_cnt => scalar keys %{$self->session('cart')}
        });
    }
    my $referrer = $self->req->headers->referrer;
    $self->redirect_to($referrer);
}

sub delete {
    my $self = shift;

    my $product = $self->stash('product');
    my $count   = $self->session('cart')->{$product->{id}};

    $self->session->{cart_price} -= $count * $product->{price};
    delete $self->session('cart')->{$product->{id}};

    ####
    # long js response
    #

    my $header  = $self->req->headers->header('X-Requested-With');
    if ($header && $header eq 'XMLHttpRequest') {
        my $cart_total = 0;
        $cart_total   += $_ for values %{$self->session('cart')};
        return $self->render(
            json => {
                cart       => $cart_total,
                cart_price => $self->session('cart_price')
            }
        );
    }
    my $referrer = $self->req->headers->referrer;
    $self->redirect_to($referrer);
}

sub submit {
    my $self = shift;

    my $params = $self->req->params->to_hash;

    my %cart;
    for my $param_name ( grep { m/quantity/msx } keys %$params ) {
        my $new_quantity = delete $params->{ $param_name };
        my ( $item_id )  = int $param_name;
        $cart{ $item_id } = $new_quantity;
    }
    my $user_id  = $self->dbi->model('users')->create_user( $params );
    my $order_id = $self->dbi->model('orders')->add( $user_id );

    if ( defined $order_id ) {
        $self->dbi->model('orders_products')->add( $order_id, \%cart );
        $self->send_adm_mail( $order_id );
        $self->send_user_mail( $order_id );

        $self->session( cart => {} );
        $self->session( cart_price => 0 );
        $self->render( json => {
            ecommerce_data => {
                purchase => {
                    actionField => { id => $order_id ."" },
                    products    => [ map { { id => $_, quantity => $cart{$_} } } keys %cart ]
                }
            },
            result => 1,
            message => 'Благодарим за заказ, наш специалист свяжется с вами в ближайшее время.'
        } );
    }
    else {
        $self->render( json => { result => 0, message => 'Произошла ошибка. Попробуйте снова или позвоните нам.' } );
    }

    return;
}

sub order {
    my $self = shift;
    my $user = $self->req->params->to_hash;
    my $cart = $self->session('cart') || {};    
    my $vc   = $self->validation;
    $vc->required('contact_name')->size(1, 255);
    $vc->required('email')->email;
    $vc->required('phone')->phone;

    if ($vc->has_error) {
        my @product_ids          = keys %$cart;
        my $product_list         = $self->dbi->model('products')->cart_list(@product_ids);

        my $total;
        my @cart = map {
            my $product         = $_;
            my $ordered         = $cart->{$product->{id}};
            my $sum             = $ordered * $product->{price};
            $total             += $sum; 
            $product->{sum}     = $sum;
            $product->{ordered} = $ordered;
            $product;
        } @$product_list;

        $self->stash(cart => \@cart, total => $total);
        return $self->render(template => 'cart/show_cart');
    }

    my $user_id  = $self->dbi->model('users')->create_user($user);
    my $order_id = $self->dbi->model('orders')->add($user_id);

    if (defined $order_id) {
        my $cart = $self->session('cart');
        my $res  = $self->dbi->model('orders_products')->add($order_id, $cart);

        # $self->send_adm_mail($order_id);
        # $self->send_user_mail( $order_id );
        $self->flash(message => 'Ваш заказ принят. Наш менеджер свяжется с Вами в течение суток и расскажет как Вы можете забрать заказ. Спасибо! Хорошего дня!');
        $self->session(cart       => {});
        $self->session(cart_price => 0);
    }
    else {
        $self->flash(message => "Произошла ошибка попробуйте повторить сново.");
    }

    $self->redirect_to('/');
}

sub send_user_mail {
    my ( $self, $order_id ) = @_;

    my $mail_template = <<__MAIL__;
<body>
Здравствуйте, [% user.contact_name %]! <br/>
Вы сделали заказ на нашем сайте. Менеджер свяжется с Вами в ближайшее время.
<br/><br/>
Благодарим за заказ.
<br/><br/>
Ваш заказ:
<br/>
<table>
  <thead>
    <tr>
      <th width="100px">Код</th>
      <th style="text-align:left;">Наименование</th>
      <th style="text-align:center;" width="100px;">Количество</th>
      <th style="text-align:left;" width="100px;">Цена</th>
    </tr>
  </thead>
[% FOREACH position IN positions %]
<tr>
  <td>[% position.code %]</td>
  <td>[% position.name %]</td>
  <td style="text-align:center">[% position.quantity %]</td>
  <td>[% position.price %] р.</td>
</tr>
[% END %]
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th>Итого:</th>
      <th style="text-align:left;">[% total %] р.</th>
    </tr>
  </tfoot>
</table>
</body>
__MAIL__

    my $order           = $self->dbi->model('orders')->select( where => { id => $order_id } )->fetch_hash;
    my $user            = $self->dbi->model('users')->get_user( $order->{user_id} );
    my $order_positions = $self->dbi->model('orders')->get_order( $order_id );
    my $total           = sum map { $_->{quantity} * $_->{price} } @$order_positions;

    my $mail_body = '';
    my $t = Template::Tiny->new;
    $t->process(
        \$mail_template,
        {
            user      => $user,
            positions => $order_positions,
            total     => $total,
        },
        \$mail_body
    );

    my $msg = MIME::Lite->new(
        From    => 'noreply@ministerstvo-podarkov.com',
        To      => $user->{email},
        Type    => 'text/html',
        Subject => '[Министерство подарков] Заказ оформлен',
        Data    => $mail_body,
    );
    $msg->attr("content-type.charset" => "UTF-8");

    my $rres = $msg->send();

    return;
}

sub send_adm_mail {
    my ($self, $order_id) = @_;

    my $order_url = $self->url_for('mda_orders_show' , id => $order_id);
    my $data = <<HTML;
<body>
Оформлен новый заказ на сайте.
Что бы посмотреть его проследуйте по ссылке: <a href="http://ministerstvo-podarkov.com/managment/orders/show/$order_id">http://ministerstvo-podarkov.com</a>
</body>
HTML

    my $msg = MIME::Lite->new(
         From    => 'noreply@ministerstvo-podarkov.com',
         To      => 'min.podarkov@mail.ru',
         Cc      => 'twinhooker@gmail.com',
         Type    => 'text/html',
         Subject => '[Министерство подарков] Новый заказ',
         Data    => $data
    );
    $msg->attr("content-type.charset" => "UTF-8");

    my $rres = $msg->send();
}

sub clear {
    my $self = shift;
    $self->session(cart => {});
    $self->redirect_to($self->req->headers->referrer);
}

1;
