#!/usr/bin/env perl
use common::sense;
use MIME::Lite;
use DBIx::Connector;
use utf8;
use Encode qw/encode decode/;
use MIME::Base64;
use Mojo::URL;

$MIME::Lite::AUTO_CONTENT_TYPE = 1;

my $data_source  = "dbi:mysql:database=minpod";
my $user         = 'root';
my $password     = '';
my $conn_options = {
    RaiseError        => 1,
    AutoCommit        => 1,
    mysql_enable_utf8 => 1,    
};

my $connector = DBIx::Connector->new(
    $data_source,
    $user,
    $password,
    $conn_options
);

my $dbh = $connector->dbh;

my $query = q{select mail_email};


my $query = "select mr.tid, mr.mid, mt.title, mt.body, me.email
             from mmail_req mr
             join mmail_templates mt on mr.tid = mt.id
             join mmail_emails me on mr.mid = me.id
             where not exists (select null from mmail_sent as ms
                               where mr.tid = ms.tid and me.id = ms.mid)";
my $res = $dbh->selectall_arrayref($query, {Slice => {}});

my $unscribe_text = $dbh->selectrow_arrayref("select value from settings where name = 'unscribe_text'")->[0];

say $unscribe_text;

for my $email (@$res)
{
    my $attach = $dbh->selectall_arrayref("select path, content_type, name from mmail_file where tid = ?",
                                           {Slice => {}},
                                           $email->{tid});    
    
    
    my $unscribe_url = unscribe_url($email->{email});
    
    #say get_unscribe_text($unscribe_text, $unscribe_url, $email);    
    my $msg = MIME::Lite->new(
        From    => 'minpod@ministerstvo-podarkov.com',   
        To      => $email->{email},
        Subject =>  "=?UTF-8?B?" . encode_base64(encode("utf8", $email->{title}), "") . "?=",
        Type => 'text/html; charset=UTF-8',
        Data => encode("utf8", $email->{body}) . encode("utf8", get_unscribe_text($unscribe_text, $unscribe_url)),
    );
    $msg->add('List-Unsubscribe' => "<$unscribe_url>");
    for my $file (@$attach)
    {
        $msg->attach(
            Type     => 'AUTO', 
            Path     => $file->{path},
            Filename => $file->{name}
            );
    }
    say "send ok" if $msg->send();
    
    $dbh->do('insert into mmail_sent(tid, mid) values (?, ?)', undef, $email->{tid}, $email->{mid});
}

sub unscribe_url
{
    my $email = shift;
    
    my $hash = encode_base64($email);
    
    my $unscribe_url = Mojo::URL->new('http://xn----8sbeciduaxmmpbdkmhicrd.xn--p1ai');
    $unscribe_url->path('/unscribe/' . $hash);
    
    $unscribe_url->to_abs;
}

sub get_unscribe_text
{
    my ($unscribe_text, $unscribe_url) = @_;
    
    
    #say $unscribe_url;
    my ($hr_val)   = $unscribe_text =~ m/<<(\w+)>>/;
    my $unscribe_a = qq~<a href="$unscribe_url">$hr_val</a>~;
    $unscribe_text =~ s/<<\w+>>/$unscribe_a/e;
    
    $unscribe_text = qq~<span style="font-size:small">$unscribe_text</span>~;
}
