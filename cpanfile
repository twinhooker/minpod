requires 'Mojolicious';
requires 'Exception::Class';
requires 'common::sense';
requires 'Mojolicious::Plugin::Authentication';
requires 'Mojolicious::Plugin::Validator';
requires 'DBIx::Custom';
requires 'DBIx::Connector';
requires 'Email::Valid';
requires 'List::MoreUtils';
requires 'DBIx::Tree::NestedSet';
requires 'GD';
requires 'Imager';
requires 'Text::CSV';
requires 'IO::String';
requires 'Excel::Writer::XLSX';
requires 'Path::Tiny';
requires 'Readonly';
requires 'Image::Imlib2';
requires 'MIME::Lite';
requires 'JSON::XS';
requires 'Template::Tiny';
requires 'IO::Socket::SSL';
requires 'Plack';

configure => sub {
    requires 'latest';
    requires 'ExtUtils::MY_Metafile';
};
