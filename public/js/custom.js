function hudMsg(type, message, timeOut) {
    $('.hudmsg').remove();
    if( ! timeOut ) {
        timeOut = 3000;
    }
    var timeId = new Date().getTime();
    if( type != '' && message != '' ) {
        $('<div class="hudmsg '+type+'" id="msg_'+timeId+'"><img src="/images/adminium/msg_'+type+'.png" alt="" />'+message+'</div>').hide().appendTo('body').fadeIn();
        var timer = setTimeout(
            function() {
                $('.hudmsg#msg_'+timeId+'').fadeOut('slow', function() {
                    $(this).remove();
                });
            }, timeOut
        );
    }
}
$(document).ready(function () {
    $('.info_message').click(function() { $(this).fadeOut(); });
});

// On/Off switch button
$('.onoffbtn').each(function() {
	$(this).wrap('<span class="onoff_box" />');
	
	if( $(this).is(':checked') ) {
		$(this).parents('span').addClass('checked');
	}		
});		

$('.onoff_box').live('click', function() {
	var onoffSwitch = $(this);
	var chckBox = $(this).find('input.onoffbtn');
	
	if( chckBox.is(':checked') ) {
		onoffSwitch.animate({ 'background-position-x': '0' }, 100, function() {
			chckBox.removeAttr('checked');
			$(this).removeClass('checked');
		});
	} else {		
		onoffSwitch.animate({ 'background-position-x': '-40px' }, 100, function() {
			chckBox.attr('checked', 'checked');
			$(this).addClass('checked');
		});
	}
});
