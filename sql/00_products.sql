CREATE TABLE products (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    cat_id INT NOT NULL,
    name VARCHAR (255) NOT NULL,
    code VARCHAR (10),
    price INT,
    packing VARCHAR(255),
    img VARCHAR(255),
    img_small VARCHAR(255),
    meta_tags VARCHAR(255),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE categories (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name VARCHAR (255) NOT NULL,
    description TEXT,
    meta_tags VARCHAR(255),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE articles (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    cat_id INT NOT NULL,
    title VARCHAR (255) NOT NULL,
    preview TEXT,
    body TEXT,
    meta_tags VARCHAR(255),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE  users (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    email VARCHAR (255),
    password VARCHAR (255),
    firstname VARCHAR (255),
    secondname VARCHAR (255),
    surname VARCHAR (255),
    phone VARCHAR (255),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


CREATE TABLE orders (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    user_id MEDIUMINT NOT NULL,
    created DATETIME,
    updated DATETIME,
    progress SMALLINT DEFAULT 0,
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE orders_products (
    order_id MEDIUMINT NOT NULL,
    product_id MEDIUMINT NOT NULL,
    quantity MEDIUMINT NOT NULL
);

CREATE TABLE mmail_req
(
    tid int not null,
    mid int not null
);

CREATE TABLE mmail_sent
(
    tid int not null,
    mid int not null
);
CREATE TABLE mmail_templates
(
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    title VARCHAR(255),
    body  TEXT,
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;
CREATE TABLE mmail_file
(
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    tid int not null,
    path text,
    content_type varchar(50),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS mmail_emails;
CREATE TABLE mmail_emails
(
    id INT NOT NULL AUTO_INCREMENT,
    gid INT NOT NULL,
    email varchar(100) unique,
    send boolean,
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS mmail_groups;
CREATE TABLE mmail_groups
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(25),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS settings;
CREATE TABLE settings
(
    name varchar(64) unique,
    value text
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS mmail_attach;
CREATE TABLE mmail_attach
(
    id INT NOT NULL AUTO_INCREMENT,
    filepath varchar(255),
    filename varchar(255),
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;




create table trigrams (
  trigram char(3) primary key
);

drop table trigram_matches;
create table trigram_matches (
  trigram char(3),
  product_id MEDIUMINT(9) NOT NULL,
  primary key (trigram, product_id),
  foreign key (product_id) references products(id)
);


insert into trigram_matches
  select t.trigram, d.id
  from trigrams t join products d
    on d.name like concat('%', t.trigram, '%');


---asdfasdfasdf ---


CREATE TABLE new_products (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    product_id int(11) NOT NULL,
    sort_num mediumint(9) DEFAULT NULL,
    primary key (id)
);

    
CREATE TABLE `products` (   
      `id` mediumint(9) NOT NULL AUTO_INCREMENT,
      `cat_id` int(11) NOT NULL,
      `name` varchar(255) NOT NULL,
      `code` varchar(10) DEFAULT NULL,
      `packing` varchar(255) DEFAULT NULL,
      `img` varchar(255) DEFAULT NULL,
      `img_small` varchar(255) DEFAULT NULL,
      `meta_tags` varchar(255) DEFAULT NULL,
      `price` decimal(8,2) DEFAULT NULL,
      `material` varchar(255) DEFAULT NULL,
      `height` varchar(255) DEFAULT NULL,
      `description` varchar(255) DEFAULT NULL,
      `sort_num` mediumint(9) DEFAULT NULL,
      `in_stock` tinyint(1) DEFAULT '1',
      PRIMARY KEY (`id`),
      FULLTEXT KEY `products_fts_idx` (`name`,`code`,`description`),
      FULLTEXT KEY `products_fts_name_idx` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

create table images (
    id         int NOT NULL AUTO_INCREMENT,
    thumb_url  text,
    img_url    text,
    thumb_path text,
    img_path   text,
    primary key (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

create table tagged_products (
    id         int NOT NULL AUTO_INCREMENT,
    product_id int,
    tag_id     int,
    sort_num   int default null,
    primary key  (`id`)
);
create table tags (
    id       int NOT NULL  AUTO_INCREMENT,
    name     text,
    sort_num int default null,
    primary key (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
