create table images (
    id         int not null auto_increment,
    thumb_url  text,
    img_url    text,
    img_path   text,
    thumb_path text
) DEFAULT CHARSET=utf8;
