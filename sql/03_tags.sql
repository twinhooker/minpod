alter table tags add column show_on_main bool default false;
alter table tags add column show_on_product bool default false;
alter table tags add column product_img_id int default null;
